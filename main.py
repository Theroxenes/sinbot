import asyncio
import datetime
import importlib
import logging
import os
import sys

import asyncpg
import discord
from discord.ext.commands import Bot

from cogs.ext import checks, pg_init

async def get_token(sess_name):

    temp_conn = await asyncpg.connect(dsn='postgres://sinbot_user@localhost:5432/sinbot')
    await pg_init.make_tables(temp_conn)
    rows = await temp_conn.fetchrow('SELECT * FROM tokens WHERE tokens.name = $1', sess_name)
    sess_token, prefix = rows['token'], str(rows.get('prefix', 'test '))
    if not sess_token:
        print('No token in database. Exiting.')
        sys.exit()

    return sess_token, prefix

async def get_prefix(bot, message):

    """Checks a bunch of shit and returns custom prefixes per-guild if they exist"""

    if message.guild:
        return bot.guild_prefs.get(message.guild.id, {}).get('prefixes', bot.default_prefix) or bot.default_prefix

    return bot.default_prefix

async def recover_prefixes(bot):

    for guild in bot.guilds:

        row = await bot.pg_session.fetchrow("SELECT prefixes FROM guild_prefs WHERE guild_id = $1", guild.id)
        if guild.id not in bot.guild_prefs:
            bot.guild_prefs.update({guild.id: {}})

        if row:
            bot.guild_prefs.update({guild.id: {'prefixes': row.get('prefixes', None)}})


name = 'prod'  # test or prod

intents = discord.Intents.default()
intents.members = True
intents.presences = True
intents.messages = True
intents.message_content = True
intents.moderation = True

token, prefix = asyncio.run(get_token(name))

exclude_cogs = []
startup_cogs = ['cogs.' + x.replace('.py', '') for x in os.listdir('cogs') if (x.endswith('.py')) & (x not in exclude_cogs)]


class Sinbot(Bot):

    async def start_cogs(self):
        for cog in startup_cogs:
            try:
                await bot.load_extension(cog)
            except Exception as e:
                exc = '{}: {}'.format(type(e).__name__, e)
                print('Failed to load extension {}\n{}'.format(cog, exc))
    async def setup_hook(self) -> None:

        self.pg_session = await asyncpg.create_pool(dsn='postgres://sinbot_user@localhost:5432/sinbot')
        await self.start_cogs()


bot = Sinbot(command_prefix=get_prefix, case_insensitive=True, max_messages=50000, intents=intents)
bot.default_prefix = prefix
bot.guild_prefs = {}

#if name == 'test':
#    bot.command_prefix.append('test ')

bot.filter = {}
bot.filtered_messages = []
bot.tasks = []
bot.log_task = []


def log_shunt():
    stats = os.stat('cogs/data/sinbot.log')
    if stats.st_size > (1024 * 50):
        time_make = str(datetime.datetime.fromtimestamp(stats.st_ctime))[:10]
        time_now = str(datetime.datetime.now())[:10]
        try:
            os.rename('cogs/data/sinbot.log', 'cogs/data/logs/{}_to_{}_sinbot.log'.format(time_make, time_now))
        except FileNotFoundError:
            pass


try:
    log_shunt()
except (PermissionError, FileExistsError):
    pass

logging.basicConfig(filename="cogs/data/sinbot.log", format='%(asctime)s:%(levelname)s:%(name)s: %(message)s')
stderrLogger = logging.StreamHandler()
logging.getLogger().addHandler(stderrLogger)

@bot.check
async def bot_check(ctx):
    return (not ctx.author.bot) & (await checks.command_check(ctx, True))


@bot.event
async def on_ready():

    print('Logged in as {}'.format(bot.user.name))
    print(bot.user.id)
    print('------------')
    await bot.change_presence(activity=discord.Game(name='Edging.'), status=discord.Status.online)
    await recover_prefixes(bot)

@bot.event
async def on_message(message):
    try:
        if bool(message.guild) & (message.author.id != bot.user.id):
            if not message.channel.permissions_for(message.author).administrator:
                filter = bot.filter.get(message.guild.id, [])
                if not filter:
                    pass
                else:
                    if any([x for x in filter if x in message.content.lower()]):
                        bot.filtered_messages.append(message.id)
                        return await message.delete()
    except (KeyError, AttributeError):
        pass

    await bot.process_commands(message)


@bot.command(hidden=True)
@discord.ext.commands.is_owner()
async def cogreload(ctx):

    print(bot.tasks)
    for t in bot.tasks:
        t.cancel()
        try:
            await t
        except asyncio.CancelledError:
            print(f'Task {t} cancelled.')

    bot.tasks = []

    ext = ['cogs.ext.' + x.replace('.py', '') for x in os.listdir('cogs/ext/') if x.endswith('.py')]
    data = ['cogs.data.' + x.replace('.py', '') for x in os.listdir('cogs/data/') if x.endswith('.py')]
    classes = ['cogs.helpers.' + x.replace('.py', '') for x in os.listdir('cogs/helpers/') if x.endswith('.py')]

    for module in ext + data + classes:
        try:
            importlib.reload(sys.modules[module])
        except KeyError:
            pass

    cognames = [c.__module__ for c in bot.cogs.values()]
    for cog in cognames:
        await bot.unload_extension(cog)
        await bot.load_extension(cog)

    #await bot.tree.sync()

    return await ctx.send('Reloaded all cogs.')

@bot.command(hidden=True)
@discord.ext.commands.is_owner()
async def commandsync(ctx):

    await bot.tree.sync()
    await ctx.send('Commands re-synced.')

if __name__ == "__main__":
    bot.run(token)
    asyncio.get_event_loop().create_task(bot.pg_session.close())
