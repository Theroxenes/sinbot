import discord
from discord.ext import commands


class MyHelpCommand(commands.DefaultHelpCommand):

    def __init__(self, **options):
        super().__init__(**options)
        self.paginator = commands.Paginator(prefix='```makefile', suffix='```', max_size=2000, linesep='\n')


class HelpCog(commands.Cog):

    def __init__(self, bot):

        self.bot = bot
        self._original_help_command = bot.help_command
        bot.help_command = MyHelpCommand()
        bot.help_command.cog = self

    def cog_unload(self):
        self.bot.help_command = self._original_help_command


async def setup(bot):
    await bot.add_cog(HelpCog(bot))
