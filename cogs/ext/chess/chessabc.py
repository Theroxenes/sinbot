from enum import Enum
from typing import Union, Self, List


def up(inp_str: str) -> str:
    return inp_str.upper()


def down(inp_str: str) -> str:
    return inp_str.lower()


class IllegalMove(Exception):
    pass


class ChessColor(Enum):
    WHITE = 0
    BLACK = 1


class ChessCoordinate(tuple):

    rank: int
    file: int

    def __new__(cls, rank: int=0, file:int=0):
        ret = super().__new__(cls, (rank, file))
        ret.rank = rank
        ret.file = file
        return ret
    #def __init__(self, seq=tuple(0, 0)):
#
    #    if len(seq) != 2:
    #        raise ValueError('ChessCoordinate must have length of 2.')
    #    super().__init__(seq)
    #    self.rank, self.file = self

    @classmethod
    def from_abs_position(cls, abs_position):

        return cls(rank=abs_position % 8, file=abs_position // 8)

    @property
    def to_abs_position(self) -> int:
        return 8 * self.file + self.rank

    @property
    def square_color(self) -> ChessColor:
        return ChessColor((1 + self.to_abs_position) % 2)

    @property
    def coordinate_algebraic(self) -> str:
        return f'{chr(self.rank + 97)}{self.file + 1}'


class BasePiece:

    name: str = 'Undefined'
    shortname: str = 'Undefined'
    color: ChessColor = ChessColor.WHITE
    position: ChessCoordinate = ChessCoordinate()
    captured: bool = False
    display_color: int = 0x000000
    display_image: Union[str, None] = None
    has_moved: bool = False

    def __init__(self, color: Union[int, ChessColor] = 0, position: ChessCoordinate = ChessCoordinate()):

        self.color = ChessColor(color)
        self.position = position

    @property
    def rank(self) -> int:
        return self.position[0]

    @property
    def file(self) -> int:
        return self.position[1]

    @property
    def fen_name(self):
        meth = {ChessColor.BLACK: up, ChessColor.WHITE: down}.get(self.color)
        return meth(getattr(self, 'shortname', 'P'))

    def become_captured(self):
        self.captured = True

    def promote(self):
        raise NotImplemented

    def render(self):
        raise NotImplemented

    def check_if_pinned_move(self, moveto: ChessCoordinate, board: List[Self]) -> bool:
        raise NotImplemented

    def get_base_legal_squares(self) -> List[ChessCoordinate]:
        raise NotImplemented

    def get_enemy_unblocked_squares(self, board: List[Self]) -> List[ChessCoordinate]:
        raise NotImplemented

    def get_friendly_unblocked_squares(self, board: List[Self]) -> List[ChessCoordinate]:
        raise NotImplemented

    def get_all_unblocked_squares(self, board: List[Self]) -> List[ChessCoordinate]:
        return [coord for coord in self.get_base_legal_squares() if coord in self.get_enemy_unblocked_squares(board)
                and coord in self.get_friendly_unblocked_squares(board)]

    def check_move_legality(self, moveto: ChessCoordinate, board: List[Self]) -> bool:

        if moveto != self.position and moveto in self.get_base_legal_squares():
            if moveto in self.get_all_unblocked_squares(board):
                return self.check_if_pinned_move(moveto, board)
            else:
                return False
        else:
            return False

    def debug_legal_move(self, movefrom: ChessCoordinate, moveto: ChessCoordinate, board: List[Self]) -> bool:

        if self.position != movefrom or self.position == moveto or moveto not in self.get_base_legal_squares():
            raise IllegalMove(f'{self.name} cannot move to {moveto.coordinate_algebraic}.')

        if moveto not in self.get_all_unblocked_squares(board):
            raise IllegalMove(f'{self.shortname}{self.position.coordinate_algebraic}{moveto.coordinate_algebraic} '
                              f'would collide with a piece.')

        if not self.check_if_pinned_move(moveto, board):
            raise IllegalMove(f'{self.name} on {self.position.coordinate_algebraic} is pinned.')

        return True

    def move(self, move):
        raise NotImplemented

    def __str__(self) -> str:

        return f'{self.name} at {self.position.coordinate_algebraic}'

    def __repr__(self) -> str:

        return str(self)