from typing import Self, Union, List

from chessabc import BasePiece, ChessColor, ChessCoordinate, IllegalMove


class ChessMove:

    def __init__(self, piece: BasePiece, moveto: ChessCoordinate, *,
                 capture: bool = False, check: bool = False, mate: bool = False):
        self.piece = piece
        self.player: ChessColor = piece.color
        self.movefrom = piece.position
        self.moveto: ChessCoordinate = moveto
        self.capture: bool = capture
        self.check: bool = check
        self.mate: bool = mate

    def set_capture_check_mate(self, board: List[BasePiece]):

        raise NotImplemented

    def to_algebraic(self, board: List[BasePiece], ignore_legality: bool=False):

        check_sign = f'{"" if not self.check and not self.mate else "#" if self.mate else "+"}'
        shortname = self.piece.shortname

        if shortname == 'P':
            shortname = ''

        if not ignore_legality:
            needs_disambiguation = [p for p in board if p.check_if_legal_move(self, board)]
        else:
            needs_disambiguation = []

        if not needs_disambiguation:
            return f'{shortname}{"x" if self.capture else ""}{self.moveto.coordinate_algebraic}{check_sign}'

        needs_single_disambiguation = [p for p in needs_disambiguation if p.file == self.movefrom.file or p.rank == self.movefrom.rank]

        if len(needs_single_disambiguation) <= 1:
            disambiguator = chr(self.movefrom.file + 97)

        else:
            disambiguator = f'{chr(self.movefrom.file + 97)}{self.movefrom.rank + 1}'

        return f'{shortname}{disambiguator}{"x" if self.capture else ""}{self.moveto.coordinate_algebraic}{check_sign}'

    @classmethod
    def from_algebraic(cls, alg_input: str, board: List[BasePiece], moving_color: ChessColor=ChessColor.WHITE):

        piece_map = {
            'R': Rook,
            'N': Knight,
            'B': Bishop,
            'Q': Queen,
            'K': King,
            'P': Pawn
        }

        if alg_input.startswith(('R', 'N', 'B', 'Q', 'K', 'P')):
            piece_code = alg_input[0]
            alg_input = alg_input[1:]
            just_move = alg_input.replace('x', '').replace('+', '').replace('#', '')
            potential_pieces = [p for p in board if isinstance(p, piece_map.get(piece_code)) and p.color == moving_color]
        elif alg_input[0] == alg_input[0].lower():
            piece_code = 'P'
            just_move = alg_input.replace('x', '').replace('+', '').replace('#', '')
            rank = ord(just_move[0]) - 97
            print(rank)
            print([p.rank for p in board])
            potential_pieces = [p for p in board if isinstance(p, piece_map.get(piece_code)) and p.rank == rank and p.color == moving_color]
        else:
            raise IllegalMove(f'{alg_input[0]} is not a legal starting character for algebraic notation.')

        if len(just_move) == 2:

            destination_rank, destination_file = ord(just_move[0]) - 97, int(just_move[1]) - 1

        elif len(just_move) == 3:

            destination_rank, destination_file = ord(just_move[1]) - 97, int(just_move[2]) - 1
            source_file = int(just_move[0]) - 1
            potential_pieces = [p for p in potential_pieces if p.file == source_file]

            if len(potential_pieces) > 1:
                raise IllegalMove(f'Insufficient disambiguation for this board state.')

        elif len(just_move) == 4:

            destination_rank, destination_file = ord(just_move[2]) - 97, int(just_move[3]) - 1
            source_rank, source_file = ord(just_move[0]) - 97, int(just_move[1]) - 1
            potential_pieces = [p for p in potential_pieces if p.file == source_file and p.rank == source_rank]
            if len(potential_pieces) > 1:
                raise IllegalMove(f'Insufficient disambiguation for this board state (this should never happen).')

        else:
            raise IllegalMove(f'Invalid algebraic notation for this board state.')

        moveto = ChessCoordinate(destination_rank, destination_file)
        print(f'Moving to {moveto.coordinate_algebraic}')
        print(potential_pieces)
        [print([c.coordinate_algebraic for c in p.get_enemy_unblocked_squares(board)]) for p in potential_pieces]
        potential_pieces = [p for p in potential_pieces if moveto in p.get_all_unblocked_squares(board)]

        #except IllegalMove as exc:
        #    raise exc
        #print(potential_pieces)
        if len(potential_pieces) == 1:
            ret = cls(potential_pieces[0], moveto)
        else:
            raise IllegalMove('Woops.')
        #ret.set_capture_check_mate(board)

        return ret


class ChessPiece(BasePiece):

    def check_if_pinned_move(self, moveto: ChessCoordinate, board: List[BasePiece]) -> bool:

        #return False

        test_board: List[BasePiece] = [p for p in board if p.position != self.position and not isinstance(p, Knight)][:]
        test_board.append(self.__class__(self.color, moveto))
        try:
            my_king = [p for p in test_board if p.name == 'King' and p.color == self.color][0]
        except IndexError:
            return True
        for piece in test_board:
            if piece.color == self.color:
                continue
            else:
                try:
                    if piece.check_if_legal_move(piece.position, my_king.position, test_board):
                        return False
                except IllegalMove:
                    pass
        return True

    def get_base_legal_squares(self) -> List[ChessCoordinate]:
        raise NotImplemented

    def get_enemy_unblocked_squares(self, board: List[Self]) -> List[ChessCoordinate]:
        raise NotImplemented

    def get_friendly_unblocked_squares(self, board: List[Self]) -> List[ChessCoordinate]:
        raise NotImplemented

    def move(self, move: ChessMove) -> ChessMove:

        #self.check_if_legal_move(move, board)
        self.position = move.moveto
        self.has_moved = True
        return move


class Pawn(ChessPiece):
    name = 'Pawn'
    shortname = 'P'

    def get_base_legal_squares(self) -> List[ChessCoordinate]:

        if self.has_moved:
            return [ChessCoordinate(self.rank, self.file + 1)]
        else:
            return [ChessCoordinate(self.rank, self.file + 1), ChessCoordinate(self.rank, self.file + 2)]

    #def check_if_legal_move(self, move: ChessMove, board: List[BasePiece]) -> bool:
    #    raise NotImplemented


class Rook(ChessPiece):

    name = 'Rook'
    shortname = 'R'

    def get_base_legal_squares(self) -> List[ChessCoordinate]:

        out = []
        for index in range(8):
            out.append(ChessCoordinate(self.rank, index))
            out.append(ChessCoordinate(index, self.file))
        out.remove(self.position)
        return out

    def get_enemy_unblocked_squares(self, board: List[BasePiece]) -> List[ChessCoordinate]:

        enemies = [p for p in board if p.color != self.color and (p.rank == self.rank or p.file == self.file)]
        if not enemies:
            return self.get_base_legal_squares()

        # TODO redo this
        e_ranks = [p.rank for p in enemies if p.file == self.file]
        e_files = [p.file for p in enemies if p.rank == self.rank]

        if e_ranks:
            min_blocked_rank = min(7, min(e_ranks))
            max_blocked_rank = max(0, max(e_ranks))
        else:
            min_blocked_rank = 0
            max_blocked_rank = 7

        if e_files:
            min_blocked_file = max(0, min(e_files))
            max_blocked_file = min(7, max(e_files))
        else:
            min_blocked_file = 0
            max_blocked_file = 7

        right_projection = [ChessCoordinate(rank, self.file) for rank in range(self.rank + 1, 8) if rank <= min_blocked_rank]
        left_projection = [ChessCoordinate(rank, self.file) for rank in range(0, self.rank) if rank >= max_blocked_rank]

        up_projection = [ChessCoordinate(self.rank, file) for file in range(self.file + 1, 8) if file <= min_blocked_file]
        down_projection = [ChessCoordinate(self.rank, file) for file in range(0, self.file) if file >= max_blocked_file]

        combined = right_projection + left_projection + up_projection + down_projection
        return list(set(combined))

    def get_friendly_unblocked_squares(self, board: List[BasePiece]) -> List[ChessCoordinate]:

        friendlies = [p for p in board if p.color == self.color and (p.rank == self.rank or p.file == self.file)]

        return self.get_base_legal_squares()


class Knight(ChessPiece):
    name = 'Knight'
    shortname = 'N'

    #def check_if_legal_move(self, move: ChessMove, board: List[BasePiece]) -> bool:
    #    raise NotImplemented


class Bishop(ChessPiece):
    name = 'Bishop'
    shortname = 'B'

    #def check_if_legal_move(self, move: ChessMove, board: List[BasePiece]) -> bool:
    #    raise NotImplemented


class Queen(ChessPiece):
    name = 'Queen'
    shortname = 'Q'

    #def check_if_legal_move(self, move: ChessMove, board: List[BasePiece]) -> bool:
    #    raise NotImplemented


class King(ChessPiece):
    name = 'King'
    shortname = 'K'

    #def check_if_legal_move(self, move: ChessMove, board: List[BasePiece]) -> bool:
    #    raise NotImplemented


class ChessBoard:

    turn: int = 0
    movehistory: List[ChessMove] = []
    square_image = None
    square_size: int = 32
    white_display_color = None
    black_display_color = None

    piece_grid: List[Union[None, ChessPiece]] = [
        # A       B          C          D         E        F          G          H
        Rook(0), Knight(0), Bishop(0), Queen(0), King(0), Bishop(0), Knight(0), Rook(0), # 1
        Pawn(0), Pawn(0),   Pawn(0),   Pawn(0),  Pawn(0), Pawn(0),   Pawn(0),   Pawn(0), # 2
        None,    None,      None,      None,     None,    None,      None,      None,    # 3
        Rook(0), None,      None,      None,     None,    None,      None,      None,    # 4
        None,    None,      None,      None,     None,    None,      None,      None,    # 5
        None,    None,      None,      None,     None,    None,      None,      None,    # 6
        Pawn(1), Pawn(1),   Pawn(1),   Pawn(1),  Pawn(1), Pawn(1),   Pawn(1),   Pawn(1), # 7
        Rook(1), Knight(1), Bishop(1), Queen(1), King(1), Bishop(1), Knight(1), Rook(1)  # 8
    ]

    piece_list: List[ChessPiece] = []

    def __init__(self):

        for i in range(64):

            this_coordinate = ChessCoordinate.from_abs_position(i)
            this_piece = self.piece_grid[i]

            if isinstance(this_piece, ChessPiece):
                this_piece.position = this_coordinate
                self.piece_list.append(this_piece)

    @property
    def current_player(self) -> str:
        return ChessColor(self.turn % 2).name

    @property
    def fencode(self):

        output = ''

        for file in range(8):
            rank_txt = ''
            previous_blanks = 0
            for rank in range(8):
                this_coordinate = ChessCoordinate(rank=rank, file=file)
                this_piece = self.get_piece_at_coordinate(this_coordinate)
                if this_piece:
                    if previous_blanks > 0:
                        blanks_text = f'{previous_blanks}'
                    else:
                        blanks_text = ''
                    rank_txt += f'{blanks_text}{this_piece.fen_name}'
                    previous_blanks = 0
                else:
                    previous_blanks += 1
            if rank_txt:
                output += f'{rank_txt}/'
            else:
                output += f'{previous_blanks}/'
        # do some stuff for other FEN info
        return output[:-1]

    def get_piece_at_coordinate(self, position: ChessCoordinate) -> Union[None, ChessPiece]:

        for piece in self.piece_list:
            if piece.position == position:
                return piece
        return None

    def render_square(self, position: ChessCoordinate):

        square_color = position.square_color
        raise NotImplemented

    def render(self):

        for i in range(64):
            this_coordinate = ChessCoordinate.from_abs_position(i)
            square = self.render_square(this_coordinate)
            piece = self.get_piece_at_coordinate(this_coordinate)
            if isinstance(piece, ChessPiece):
                rendered_piece = piece.render()
            else:
                rendered_piece = None
        raise NotImplemented

    def __str__(self) -> str:

        return '\n'.join([' '.join([getattr(self.get_piece_at_coordinate(ChessCoordinate(rank, file)), "fen_name", "+") for rank in range(8)]) for file in range(8)])

    def __repr__(self) -> str:

        return str(self)


if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    chessboard = ChessBoard()
    print(chessboard)
    print(chessboard.fencode, '\n')
    move = ChessMove.from_algebraic('Rd4', chessboard.piece_list)
    move.piece.move(move)
    print(chessboard)
