import ast

import discord.abc
from discord.ext import commands


async def check_permissions(ctx, perms, *, check=all):
    is_owner = await ctx.bot.is_owner(ctx.author)
    if is_owner:
        return True

    resolved = ctx.channel.permissions_for(ctx.author)
    return check(getattr(resolved, name, None) == value for name, value in perms.items())


def has_permissions(*, check=all, **perms):
    async def pred(ctx):
        return await check_permissions(ctx, perms, check=check)

    return commands.check(pred)


async def check_guild_permissions(ctx, perms, *, check=all):
    is_owner = await ctx.bot.is_owner(ctx.author)
    if is_owner:
        return True

    if ctx.guild is None:
        return False

    resolved = ctx.author.guild_permissions
    return check(getattr(resolved, name, None) == value for name, value in perms.items())


def has_guild_permissions(*, check=all, **perms):
    async def pred(ctx):
        return await check_guild_permissions(ctx, perms, check=check)

    return commands.check(pred)


# These do not take channel overrides into account

def is_mod():
    async def pred(ctx):
        return await check_guild_permissions(ctx, {'manage_guild': True})

    return commands.check(pred)


def is_admin():
    async def pred(ctx):
        return await check_guild_permissions(ctx, {'administrator': True})

    return commands.check(pred)


def mod_or_permissions(**perms):
    perms['manage_guild'] = True

    async def predicate(ctx):
        return await check_guild_permissions(ctx, perms, check=any)

    return commands.check(predicate)


def admin_or_permissions(**perms):
    perms['administrator'] = True

    async def predicate(ctx):
        return await check_guild_permissions(ctx, perms, check=any)

    return commands.check(predicate)


def block_check():
    async def pred(ctx):
        with open('prefs.py', 'r') as block_read:
            blocklist = ast.literal_eval(block_read.read(-1))['blocklist']

        return not bool(ctx.message.author.id in blocklist)

    return commands.check(pred)


def dmchannel_or_admin_or_permissions(**perms):

    perms['administrator'] = True

    async def predicate(ctx):
        return isinstance(ctx.channel, discord.abc.PrivateChannel) or await check_guild_permissions(ctx, perms, check=any)

    return commands.check(predicate)


async def command_check(ctx, default):

    if not ctx.guild:
        return True

    await ctx.bot.wait_until_ready()

    if ctx.guild.id not in ctx.bot.toggled_commands:
        return default

    if (ctx.command.name in ctx.bot.toggled_commands[ctx.guild.id]):
        if ctx.bot.toggled_commands[ctx.guild.id][ctx.command.name]:
            if ctx.channel.id in ctx.bot.toggled_commands[ctx.guild.id][ctx.command.name]:
                return not(default)

    return default