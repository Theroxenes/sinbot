#import asyncpg

tables = {
    'tokens': ['name text COLLATE pg_catalog."default" NOT NULL', 'token text COLLATE pg_catalog."default" NOT NULL', 'prefix text COLLATE pg_catalog."default"', 'CONSTRAINT tokens_pkey PRIMARY KEY (name)'],
    'guild_prefs': ['guild_id bigint NOT NULL', 'link_id bigint', 'appeal_target text COLLATE pg_catalog."default"', 'prefixes text[] COLLATE pg_catalog."default"', 'inserver_appeals boolean DEFAULT false', 'filter_words text[] COLLATE pg_catalog."default"', 'twitter_fixchans bigint[]', 'twitter_fixbans bigint[]', 'CONSTRAINT guild_prefs_pkey PRIMARY KEY (guild_id)'],
    'roles': ['guild_id bigint NOT NULL', 'role_id bigint NOT NULL', 'name text COLLATE pg_catalog."default" NOT NULL', 'CONSTRAINT roles_pkey PRIMARY KEY (guild_id, role_id)'],
    'ban_data': ['user_id bigint NOT NULL', 'guild_id bigint NOT NULL', 'ban_reason text COLLATE pg_catalog."default"', 'ban_date TIMESTAMP WITH TIME ZONE NOT NULL', 'temporary boolean', 'end_time timestamp with time zone', 'ban_cmd_link text COLLATE pg_catalog."default"', 'ban_author bigint', 'tempban_resolved boolean', 'CONSTRAINT unique_ban PRIMARY KEY (user_id, guild_id, ban_date, ban_reason)'],
    'welcomes': ['guild_id bigint NOT NULL', 'channel_id bigint NOT NULL', 'message text COLLATE pg_catalog."default" NOT NULL', 'CONSTRAINT welcomes_pkey PRIMARY KEY (guild_id, channel_id)'],
    'citations': ['guild_id bigint NOT NULL', 'member_id bigint NOT NULL', 'author_id bigint NOT NULL', 'citation_time TIMESTAMP WITH TIME ZONE NOT NULL', 'citation_text TEXT NOT NULL', 'citation_link text COLLATE pg_catalog."default"', 'CONSTRAINT citations_pkey PRIMARY KEY (guild_id, member_id, citation_time)'],
    'reminders': ['reminder_text text', 'ask_time TIMESTAMP WITH TIME ZONE NOT NULL', 'remind_when TIMESTAMP WITH TIME ZONE NOT NULL', 'channel_id bigint NOT NULL', 'user_id bigint NOT NULL', 'jump_url text', 'CONSTRAINT unique_reminder PRIMARY KEY (reminder_text, ask_time, remind_when, channel_id, user_id)'],
    'feeds': ['name TEXT NOT NULL', 'destination BIGINT NOT NULL', 'header TEXT DEFAULT \'\'', 'publish BOOLEAN DEFAULT FALSE', 'dispatched TEXT[]', 'CONSTRAINT unique_feed PRIMARY KEY (name, destination)']
}

async def make_tables(conn):

    for table in tables:
        await conn.execute(f'CREATE TABLE IF NOT EXISTS public.{table}({", ".join(tables[table])})'
                           f'WITH (OIDS = FALSE)'
                           f'TABLESPACE pg_default;'
                           f'ALTER TABLE public.{table}'
                                ' OWNER to sinbot_user;')
        print(f'{table} was created or already exists.')
        for col in tables[table]:
            if 'CONSTRAINT' not in col:
                await conn.execute(f'ALTER TABLE {table} ADD COLUMN IF NOT EXISTS {col}')
