from discord import Interaction
from discord.ext import commands
import discord
import asyncpg
import aiohttp
import datetime
import re
import asyncio
import io
import textwrap
import traceback
from contextlib import redirect_stdout
from copy import deepcopy
import logging
import random
import os
from typing import Optional, Dict, List, Union

from .ext import checks
from .helpers.utilities import paginate, escape_nick, timestampformat, durationparser, chunk_list
from .helpers.classes import Citation, CustomEmbed

here = os.path.join(os.path.dirname(__file__))


class BanFlags(commands.FlagConverter, prefix='-', delimiter=' '):

    @classmethod
    def parse_flags(cls, argument: str, *, ignore_extra: bool = True) -> Dict[str, List[str]]:
        if not argument.startswith('-'):
            argument = f'-reason {argument}'
        return super().parse_flags(argument, ignore_extra=True)

    reason: Optional[str] = commands.flag(description='Reason for the ban.', default='None')
    delete: Optional[str] = commands.flag(description='Duration of messages to delete.', default='0d')


class ModActionCollision(Exception):

    pass


class ModActionMetadata:

    __slots__ = ['actiontype', 'guildid', 'memberid', 'authorid', 'reason', 'date', 'jumplink', 'delete_days', 'duration', '_valid_actiontypes']

    def __init__(self, actiontype: str, guildid: int, memberid: int, authorid: Union[int, None], reason: str, date: datetime.datetime,
                 jumplink: str = None, delete_days: datetime.timedelta = None, duration: datetime.timedelta = None):

        self._valid_actiontypes = ['ban', 'unban', 'timeout']
        if actiontype not in self._valid_actiontypes:
            raise ValueError(f'Invalid ModActionMetadata actiontype {actiontype}.')

        self.actiontype = actiontype
        self.guildid = guildid
        self.memberid = memberid
        self.authorid = authorid
        self.reason = reason
        self.date = date
        self.jumplink = jumplink
        self.delete_days = delete_days
        self.duration = duration

    def format_title(self, timeout_removed_manually: bool = False) -> str:

        if self.actiontype == 'ban':
            if not self.is_temporary:
                return 'Permanently Banned'
            else:
                return f'Temporarily Banned until {discord.utils.format_dt(self.date + self.duration)}'
        elif self.actiontype == 'timeout':
            if not timeout_removed_manually:
                return f'Timed Out until {discord.utils.format_dt(self.date + self.duration)}'
            else:
                return f'Timeout Removed'
        elif self.actiontype == 'unban':
            return 'Unbanned'

    def format_author(self, timeout_removed_manually: bool = False) -> str:

        if self.actiontype == 'ban':
            return 'Banned'
        elif self.actiontype == 'timeout':
            if not timeout_removed_manually:
                return 'Timed Out'
            else:
                return 'Timeout Removed'
        elif self.actiontype == 'unban':
            return 'Unbanned'

    @property
    def is_temporary(self):

        return bool(self.duration)

    def __str__(self):

        return ', '.join([f'{s}: {getattr(self, s, "None")}' for s in self.__slots__])

    def __repr__(self):

        return str(self)

    def __eq__(self, other):
        try:
            return all([self.reason == other.reason, self.actiontype == other.actiontype,
                        self.memberid == other.memberid, self.guildid == other.guildid])
        except AttributeError:
            return False

    def __ne__(self, other):

        return not self.__eq__(other)


class ModActionHandler:

    __slots__ = ["actionqueue", "_lock"]

    def __init__(self):

        self.actionqueue = {
            "ban": {},
            "timeout": {},
            "unban": {}
        }
        self._lock = False

    def _init_queue(self, action: ModActionMetadata = None, *, actiontype: str = None, guildid: int = None, memberid: int = None) -> List[ModActionMetadata]:
        """
        :param action: ModActionMetadata to initiate the queue for
        :param actiontype: str actiontype of 'ban', 'unban', 'timeout' to initiate the queue for
        :param guildid: int guild.id to initiate the queue for
        :param memberid: int member.id to initiate the queue for

        :return: List[ModActionMetadata] the initiated queue

        :raise: ValueError
        """
        if action:
            actiontype = action.actiontype
            guildid = action.guildid
            memberid = action.memberid

        if not all([actiontype, guildid, memberid]) or actiontype not in self.actionqueue:
            raise ValueError(f"Invalid data passed: {actiontype}, {guildid}, {memberid}")

        this_action_queue = self.actionqueue.get(actiontype, {})
        if guildid not in this_action_queue.keys():
            this_action_queue.update({guildid: {memberid: []}})
        elif memberid not in this_action_queue.get(guildid, {}).keys():
            this_action_queue.get(guildid).update({memberid: []})

        return this_action_queue.get(guildid).get(memberid)

    async def wait_until_unlocked(self):

        while self._lock:
            await asyncio.sleep(0)

    async def fetch_action(self, actiontype: str, guildid: int, memberid: int, reason: str = None) -> Union[ModActionMetadata, None]:
        """
        :param actiontype: type of action to get - ban, unban, timeout
        :param guildid: guild.id of the guild for the action
        :param memberid: target member.id
        :param reason: action reason [optional]

        :return: ModActionMetadata or None

        :raise: ModActionCollision
        """

        # LOCKING METHOD
        await self.wait_until_unlocked()

        self._lock = True
        if actiontype not in self.actionqueue:
            self._lock = False

        user_queue = self._init_queue(actiontype=actiontype, guildid=guildid, memberid=memberid)
        valid_actions = [a for a in user_queue if a.reason == reason]
        if len(valid_actions) > 1:
            self._lock = False
            raise ModActionCollision("More than 1 matching action in the specified actionqueue.")
        elif len(valid_actions) < 1:
            self._lock = False
            return None

        self._lock = False
        return valid_actions[0]

    async def add_action(self, action: ModActionMetadata) -> ModActionMetadata:

        """
        :param action: ModActionMetadata to add to the appropriate queue

        :return: List[ModActionMetadata] the queue the action was added to

        :raise: ValueError, ModActionCollision
        """

        # LOCKING METHOD
        await self.wait_until_unlocked()

        self._lock = True
        try:
            user_queue = self._init_queue(action)
        except ValueError as e:
            self._lock = False
            raise e
        if action not in user_queue:
            user_queue.append(action)
        else:
            self._lock = False
            raise ModActionCollision("Action already in queue.")

        self._lock = False
        return action

    async def remove_multiple_actions(self, *, actiontype: str, guildid: int, memberid: int, reason: str = None) -> None:

        """
        :param actiontype: str of 'ban', 'unban', 'timeout' to remove from
        :param guildid: int guild.id to remove from
        :param memberid: int member.id to remove from
        :param reason: Union[str, None] reason to filter on for removal

        :return: None
        """

        # LOCKING METHOD
        await self.wait_until_unlocked()

        self._lock = True

        user_queue = self._init_queue(actiontype=actiontype, guildid=guildid, memberid=memberid)
        new_queue = [a for a in user_queue if a.reason != reason]
        self.actionqueue.get(guildid).update({memberid: new_queue})
        self._lock = False
        return None

    async def remove_action(self, action: ModActionMetadata = None, *, actiontype: str = None, guildid: int = None, memberid: int = None,
                            reason: str = None) -> Union[ModActionMetadata, None]:

        """
        :param action: Optional[ModActionMetadata] to remove from the appropriate queue
        :param actiontype: Optional[str] of 'ban', 'unban', 'timeout' to remove from
        :param guildid: Optional[int] guild.id to remove from
        :param memberid: Optional[int] member.id to remove from
        :param reason: Optional[str] reason to filter on for removal
        :param jumplink: Optional[str] action command link to filter on for removal

        :return: Union[ModActionMetadata, None] the removed action or None if no matching action found

        :raise: IndexError, ValueError
        """

        if not any([action, all([actiontype, guildid, memberid, reason])]):
            return None

        # LOCKING METHOD
        await self.wait_until_unlocked()

        self._lock = True
        if action:
            actiontype = action.actiontype
            guildid = action.guildid
            memberid = action.memberid
            reason = action.reason

        try:
            self._lock = False
            this_action = await self.fetch_action(actiontype=actiontype, guildid=guildid, memberid=memberid, reason=reason)
            self._lock = True
        except ModActionCollision:
            self._lock = False
            await self.remove_multiple_actions(actiontype=actiontype, guildid=guildid, memberid=memberid, reason=reason)
            return None

        try:
            this_queue = self._init_queue(this_action)
        except ValueError as e:
            self._lock = False
            raise e

        try:
            index = this_queue.index(this_action)
        except ValueError as e:
            self._lock = False
            raise e

        try:
            this_queue.pop(index)
        except IndexError:
            self._lock = False
            return None

        self._lock = False
        return this_action

    def format_wrap(self, inp_str: str) -> str:

        return f'```\n{inp_str}```'

    async def format_output(self, action: ModActionMetadata, bot: commands.Bot, timeout_removed_manually: bool = False):

        """
        :param action: ModActionMetadata to generate prettified output for
        :param bot: discord.ext.commands.Bot to fetch user data
        :param timeout_removed_manually: Optional[bool] to handle timeout logic
        :return: CustomEmbed
        """

        colormap = {
            'ban': discord.Color(0x961B00),
            'unban': discord.Color(0x6BA366),
            'timeout': discord.Color(0xea681a)
        }

        user = await bot.fetch_user(action.memberid)
        try:
            author = await bot.fetch_user(action.authorid)
            if author:
                author = author.mention
        except discord.errors.HTTPException:
            author = action.authorid

        newline = '\n'
        if timeout_removed_manually:
            jumplink = f'{f"Command Jump Link: {action.jumplink}" if action.jumplink else ""}' or "Command Jump-link: Timeout Removed Manually"
        elif not action.jumplink:
            jumplink = f'Command Jump-link: Manual {action.actiontype.title()}'
        else:
            jumplink = f'{action.actiontype.title()} Command Jump-Link: {action.jumplink}'

        embed = CustomEmbed(title=f'User `{user.name}` {action.format_title(timeout_removed_manually)}:', description=user.mention,
                            color=colormap.get(action.actiontype))
        embed.add_field(name=f"{action.actiontype.title()} Information",
                        value=f'{action.format_author(timeout_removed_manually)} by: {author or "Unknown"}\n'
                              f'{jumplink}\n'
                              f'Date: {discord.utils.format_dt(action.date)}'
                              f'{f"{newline}Expires: {discord.utils.format_dt(action.date + action.duration)}" if action.duration else ""}',
                        inline=False)
        embed.add_field(name=f'{action.actiontype.title()} Reason:', value=f'{action.reason}', inline=False, wrap=self.format_wrap)
        return embed

    async def handle_event(self, action: ModActionMetadata, bot: commands.Bot,
                           timeout_removed_manually: bool = False) -> Union[CustomEmbed, None]:

        """
        :param action: ModActionMetadata to add and format
        :param bot: discord.ext.commands.Bot to fetch users
        :param timeout_removed_manually: Optional[bool] for timeout removal handling

        :return: Union[CustomEmbed, None] prettified action logging embed or None if invalid action passed
        """

        try:
            await self.add_action(action)
        except ValueError:
            return None
        except ModActionCollision:
            pass

        this_action = await self.fetch_action(action.actiontype, action.guildid, action.memberid, action.reason)
        return await self.format_output(this_action, bot, timeout_removed_manually)


class ButtonPaginator(discord.ui.View):

    def __init__(self, caller: int, page: int=0, embed_pages: List[discord.Embed]=[]):

        super().__init__()
        self.pagecount = len(embed_pages)
        self.page = page
        self.embed_pages = embed_pages
        self.timeout = 600
        self.caller = caller

    async def interaction_check(self, interaction: Interaction, /) -> bool:

        return interaction.user.id == self.caller

    @discord.ui.button(label='<<', style=discord.ButtonStyle.blurple)
    async def firstpage(self, interaction: discord.Interaction, button: discord.ui.Button):

        self.page = 0
        await interaction.response.edit_message(embed=self.embed_pages[self.page], view=self)

    @discord.ui.button(label='<', style=discord.ButtonStyle.green)
    async def backpage(self, interaction: discord.Interaction, button: discord.ui.Button):

        self.page = max(0, self.page - 1)
        await interaction.response.edit_message(embed=self.embed_pages[self.page], view=self)

    @discord.ui.button(label='>', style=discord.ButtonStyle.green)
    async def forwardpage(self, interaction: discord.Interaction, button: discord.ui.Button):

        self.page = min(self.pagecount - 1, self.page + 1)
        await interaction.response.edit_message(embed=self.embed_pages[self.page], view=self)

    @discord.ui.button(label='>>', style=discord.ButtonStyle.blurple)
    async def lastpage(self, interaction: discord.Interaction, button: discord.ui.Button):

        self.page = self.pagecount - 1
        await interaction.response.edit_message(embed=self.embed_pages[self.page], view=self)


class Moderation(discord.ext.commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.bot.badboycount = 0

        self.bot.modactionhandler = ModActionHandler()

        self.bot.toggled_commands = {}
        self.bot.tasks.append(self.bot.loop.create_task(self.recover()))
        self._last_result = None
        self.silence_channels = []
        self.welcome_channels = {}
        self.citations: Dict[int, Dict[int, List[Citation]]] = {}
        self.new_users = {}
        self.zws = u'\u200B'
        self.emojiregex = re.compile(r'<(?P<animated>a?):(?P<name>[a-zA-Z0-9_]{2,32}):(?P<id>[0-9]{18,22})>')
        self.citation_cmd = None

    async def get_flags_giveaway(self, ctx, args):

        args = [a.strip() for a in args.split(',')]

        newln = '\n'
        flags = {

            'item': None,
            'timer': 24,
            'rolelock': None,
            'ping': None,
            'winners': 1,
            'via': ctx.author,

        }
        badflags = []

        for arg in args:
            flag = arg.split(':')[0].lower()

            rest = ' '.join(arg.split(' ')[1:]).strip()
            if flag in flags:

                if flag in ['timer', 'winners']:
                    try:
                        rest = max(1, min(168, int(rest)))
                        if rest < 1:
                            raise ValueError
                    except ValueError:
                        await ctx.send(f'{flag.title()} arg not a valid integer or is less than 1.')
                        return None

                elif flag in ['rolelock', 'ping']:

                    try:
                        rest = await commands.RoleConverter().convert(ctx, rest)
                    except commands.errors.BadArgument:
                        await ctx.send(f'Role for {flag} not found.')
                        return None

                elif flag in ['via']:

                    try:
                        rest = await commands.MemberConverter().convert(ctx, rest)
                    except commands.errors.BadArgument:
                        await ctx.send(f'Member for {flag} not found.')
                        return None

                flags.update({flag: rest})

            else:

                badflags.append(flag)

        if badflags:

            await ctx.send(f'Bad flags in command: `{", ".join(badflags)}`\n'
                           f'Available flags:\n```\n{newln.join(list(flags.keys()))}```\n'
                           f'Example of properly formed command:\n'
                           f'`<prefix> giveaway item: World Domination, timer: 48, rolelock: Bot, ping: Giveaways, winners: 1`')

            return None
        return flags

    async def cog_check(self, ctx):
        if ctx.command.name == 'eval' and ctx.author.id == self.bot.owner_id:
            return True
        return bool(ctx.guild)

    async def appeal_channel_check(self, guild):
        row = await self.bot.pg_session.fetchrow('SELECT inserver_appeals FROM guild_prefs WHERE guild_id = $1',
                                                 guild.id)
        if row:
            return row['inserver_appeals']
        else:
            return False

    async def is_banned_check(self, member):

        bans = {}

        links = await self.bot.pg_session.fetchrow('SELECT guild_id FROM guild_prefs WHERE link_id = $1',
                                                   member.guild.id)
        if links:
            for link_id in links:
                row = await self.bot.pg_session.fetchrow('SELECT * FROM ban_data WHERE user_id = $1 AND guild_id = $2',
                                                         member.id, link_id)
                if row:
                    bans.update({row['guild_id']: True})

        row = await self.bot.pg_session.fetchrow('SELECT * FROM ban_data WHERE user_id = $1 AND guild_id = $1',
                                                 member.guild.id)
        if row:
            bans.update({row['guild_id']: True})

        return bans

    async def ban_timer(self, ban: ModActionMetadata):

        if ban.actiontype != 'ban' or not ban.is_temporary:
            return

        delay = ban.duration.total_seconds()
        await asyncio.sleep(max(0, delay))
        now = datetime.datetime.now()

        unban_action = ModActionMetadata('unban', ban.guildid, ban.memberid, ban.authorid, ban.reason, now, ban.jumplink)
        guild = self.bot.get_guild(ban.guildid)

        try:
            try:
                await self.bot.modactionhandler.add_action(unban_action)
            except ModActionCollision:
                pass
            await guild.unban(discord.Object(ban.memberid), reason=ban.reason)
            await self.bot.modactionhandler.remove_action(ban)
            await self.bot.pg_session.execute('UPDATE ban_data SET tempban_resolved = $1 '
                                              'WHERE user_id = $2 AND guild_id = $3 AND ban_cmd_link = $4',
                                              True, ban.memberid, ban.guildid, ban.jumplink)
        except discord.errors.NotFound:
            print(f'Manually unbanned: {ban.format_title()}')
        except (AttributeError, discord.errors.Forbidden):
            pass

    async def recover(self):

        await self.bot.wait_until_ready()

        conn = self.bot.pg_session
        prefs = await conn.fetch('SELECT * FROM guild_prefs')
        bans = await conn.fetch('SELECT * FROM ban_data WHERE temporary = True')
        welcomes = await conn.fetch('SELECT * FROM welcomes')
        citations = await conn.fetch('SELECT * FROM citations')
        self.citation_cmd = discord.utils.get(self.bot.commands, name='citation')

        for ban in bans:

            guild = self.bot.get_guild(ban['guild_id'])
            if guild:

                resolved = ban['tempban_resolved']
                end_time = ban['end_time']
                if end_time and not resolved:
                    if end_time:
                        delay = (end_time - datetime.datetime.now(tz=datetime.timezone.utc))
                    else:
                        delay = None

                    user_id = ban['user_id']
                    try:
                        g_bans = [ban async for ban in guild.bans() if ban.user.id == user_id][0]
                        user = g_bans.user
                    except IndexError:
                        user = None

                    jumplink = ban['ban_cmd_link']
                    date = ban['ban_date']
                    reason = ban['ban_reason']
                    authorid = ban['ban_author']

                    ban_action = ModActionMetadata('ban', guild.id, user_id, authorid, reason, date, jumplink, duration=delay)
                    try:
                        await self.bot.modactionhandler.add_action(ban_action)
                    except ModActionCollision:
                        pass

                    if not user:
                        continue
                    else:
                        if delay:
                            self.bot.tasks.append(self.bot.loop.create_task(self.ban_timer(ban_action)))

        for pref in prefs:
            self.bot.filter.update({pref['guild_id']: pref['filter_words']})
            self.bot.toggled_commands.update({pref['guild_id']: {}})
            for col in pref.keys():
                try:
                    if col in [f'{x.name}_command' for x in self.bot.commands]:
                        cmd_name = col.split('_')[0]
                        self.bot.toggled_commands[pref['guild_id']].update({cmd_name: pref[col] or []})
                except (IndexError, KeyError):
                    pass

        for w in welcomes:

            gid, cid, message = w
            guild = self.bot.get_guild(gid)
            if not guild:
                continue

            if gid not in self.welcome_channels:
                self.welcome_channels.update({gid: {}})
            g_dict = self.welcome_channels.get(gid)

            chan = guild.get_channel
            if not chan:
                continue

            g_dict.update({cid: message})

        for cit in citations:

            gid = cit['guild_id']
            uid = cit['member_id']
            authorid = cit['author_id']
            timestamp = cit['citation_time']
            reason = cit['citation_text']
            command_link = cit['citation_link']

            if gid not in self.citations:
                self.citations.update({gid: {}})

            prev = self.citations.get(gid, {})
            u_prev = prev.get(uid, [])

            u_prev.append(Citation(timestamp, reason, authorid, command_link))
            self.citations.get(gid, {}).update({uid: sorted(u_prev, key=lambda c: c.timestamp)})

    async def roll_giveaway(self, ctx, *, args: str, target: int=None):

        flags = await self.get_flags_giveaway(ctx, args)

        if not flags:

            return

        elif not flags.get('item'):

            return await ctx.send('No item provided for giveaway!')

        try:
            entry = await ctx.channel.fetch_message(target)
        except discord.errors.NotFound:
            return await ctx.send('Target message does not exist.')

        try:
            check = [r for r in entry.reactions if str(r) == '✅'][0]
            entrants = [m for m in [user async for user in check.users()] if (m.id not in [self.bot.user.id, flags.get("via", ctx.author).id]) & (m in ctx.guild.members)]
            if flags['rolelock']:
                entrants = [m for m in entrants if flags.get('rolelock') in getattr(m, 'roles', [])]

            if not entrants:
                raise IndexError

        except IndexError:
            return await ctx.send(f'No valid entrants in {flags.get("via", ctx.author).mention}\'s giveaway for `{flags.get("item")}`. Ending.')

        if len(entrants) < flags.get('winners'):

            flags.update({'winners': len(entrants)})

        return await ctx.send(f'{", ".join([m.mention for m in random.sample(entrants, k=flags.get("winners"))])},'
                              f' you have won the giveaway for `{flags.get("item")}`!'
                              f' Please contact {flags.get("via", ctx.author).mention} to claim your prize.')

    def paginate(self, msg):

        pagi = discord.ext.commands.Paginator(max_size=2000, prefix='```py\n')
        m_size = pagi.max_size - len(pagi.prefix) - len(pagi.suffix) - 15

        while msg:
            pagi.add_line(msg[:m_size])
            msg = msg[m_size:]

        return pagi.pages

    def citation_check(self, ctx: commands.Context, targetcitation: Citation):

        superuser = ctx.author.guild_permissions.administrator
        supersuperuser = ctx.author.id == ctx.guild.owner.id

        if supersuperuser:
            return True

        elif superuser:
            cit_author = ctx.guild.get_member(targetcitation.author)
            if not cit_author:
                return True
            elif not cit_author.guild_permissions.administrator:
                return True
            elif cit_author.id == ctx.guild.owner.id:
                return False

        return ctx.author.id == targetcitation.author

    async def sanity_check(self, ctx, sanity_check_check, init_message: str, abort_message: str):

        confirmed = False
        await ctx.send(init_message)
        try:
            msg = await ctx.bot.wait_for('message', check=sanity_check_check, timeout=60)
            if msg.content.lower() in ['yes', 'y']:
                confirmed = True
            else:
                await ctx.send(abort_message)
        except asyncio.TimeoutError:
            await ctx.send(f'{ctx.author.mention}, confirmation timed out.')

        return confirmed

    @commands.command(help='Set server ban appeal link.')
    @checks.admin_or_permissions()
    async def appeal_form(self, ctx, *, target: str):

        await self.bot.pg_session.execute('INSERT INTO guild_prefs(guild_id, appeal_target) VALUES($1, $2) '
                                          'ON CONFLICT (guild_id) DO UPDATE SET appeal_target = $2 WHERE guild_prefs.guild_id = $1',
                                          ctx.guild.id, target)

        await ctx.send('Appeal destination updated to `{}`'.format(target))

    @commands.command(help='Set in-server private appeal channels.')
    @checks.admin_or_permissions()
    async def appeal_channels(self, ctx, on_off: str = 'list'):

        if on_off == 'off':

            await self.bot.pg_session.execute('INSERT INTO guild_prefs(guild_id, inserver_appeals) VALUES($1, $2) '
                                              'ON CONFLICT (guild_id) DO UPDATE SET inserver_appeals = $2 WHERE guild_prefs.guild_id = $1',
                                              ctx.guild.id, False)

            await ctx.send('In-server appeals turned `off`.')

        elif on_off == 'on':

            await self.bot.pg_session.execute('INSERT INTO guild_prefs(guild_id, inserver_appeals) VALUES($1, $2) '
                                              'ON CONFLICT (guild_id) DO UPDATE SET inserver_appeals = $2 WHERE guild_prefs.guild_id = $1',
                                              ctx.guild.id, True)

            await ctx.send('In-server appeals turned `on`.')

        else:

            await self.bot.pg_session.execute('INSERT INTO guild_prefs(guild_id) VALUES($1)'
                                              'ON CONFLICT (guild_id) DO NOTHING', ctx.guild.id)

            setting = await self.bot.pg_session.fetchrow('SELECT inserver_appeals FROM guild_prefs WHERE guild_id = $1',
                                                         ctx.guild.id)

            if setting['inserver_appeals'] == False:
                setting = 'off'
            else:
                setting = 'on'

            await ctx.send('In-server appeals are `{}` in this server.'.format(setting))

    @commands.command()
    @checks.admin_or_permissions(ban_members=True)
    async def ban(self, ctx: commands.Context, user: discord.User = discord.ext.commands.parameter(description="User to ban."),
                  *, flags: BanFlags):

        """Usage: <prefix>ban <@user> [reason] [-delete Xd/Xh/Xm]
           Bans the specified <@user> for [reason] and deletes messages in duration specified by [-delete] flag."""

        date = ctx.message.created_at
        delete_delta = durationparser(flags.delete)

        prefs = await self.bot.pg_session.fetchrow('SELECT appeal_target FROM guild_prefs WHERE guild_id = $1',
                                                   ctx.guild.id)
        appeal_form = prefs['appeal_target']

        ban = ModActionMetadata('ban', ctx.guild.id, user.id, ctx.author.id, flags.reason, date, ctx.message.jump_url, delete_delta)
        try:
            await self.bot.modactionhandler.add_action(ban)
        except ModActionCollision:
            await self.bot.modactionhandler.remove_action(ban)
            await self.bot.modactionhandler.add_action(ban)

        try:
            await self.bot.pg_session.execute('''INSERT INTO ban_data(user_id, guild_id, ban_reason, ban_date, ban_cmd_link, ban_author)
             VALUES($1, $2, $3, $4, $5, $6)''', user.id, ctx.guild.id, flags.reason, date, ctx.message.jump_url, ctx.author.id)
        except asyncpg.exceptions.UniqueViolationError:
            pass

        if "no appeal" not in flags.reason.lower():
            try:
                msg = await user.send(f'You have been permanently banned from `{ctx.guild.name}` for reason: `{flags.reason}`\n'
                                      f'{f"This ban can be appealed here: {appeal_form}" if appeal_form else ""}')
            except (discord.Forbidden, discord.HTTPException):
                pass

        try:
            await ctx.guild.ban(user, reason=flags.reason, delete_message_seconds=int(delete_delta.total_seconds()))
            await self.bot.pg_session.execute('UPDATE ban_data SET tempban_resolved = $1 WHERE guild_id = $2 AND user_id = $3',
                                              True, ctx.guild.id, user.id)
            await ctx.invoke(self.citation_cmd, member=str(user.id), reason=f'BANNED permanently for reason: {flags.reason}', silent=True)
        except discord.errors.Forbidden:
            await self.bot.modactionhandler.remove_action(ban)
            try:
                await msg.edit(content=f'{ctx.author.mention} tried to ban you from `{ctx.guild.name}`. How rude.')
            except UnboundLocalError:
                pass
            return await ctx.send('Invalid permissions to moderate target user.')

        await ctx.send('Goodbye `{}`. <:salute:949392903380271204>'.format(user.name.replace('`', f'`{self.zws}')))

    @commands.command()
    @checks.admin_or_permissions(ban_members=True)
    async def forceban(self, ctx, user_id: int = discord.ext.commands.parameter(description="User ID to force ban."),
                       *, flags: BanFlags):

        """Usage: <prefix>forceban <user ID> [reason] [-delete Xd/Xh/Xm]
           Bans non-member specified by <user ID> with an optional [reason] and deletes messages in duration specified by [-delete] flag."""

        if not user_id:
            return await ctx.send('Please provide a user ID.')
        try:
            user_id = int(user_id)
        except ValueError:
            return await ctx.send('Invalid ID.')

        date = ctx.message.created_at
        delete_delta = durationparser(flags.delete)

        ban = ModActionMetadata('ban', ctx.guild.id, user_id, ctx.author.id, flags.reason, date, ctx.message.jump_url,
                                delete_delta)
        try:
            await self.bot.modactionhandler.add_action(ban)
        except ModActionCollision:
            await self.bot.modactionhandler.remove_action(ban)
            await self.bot.modactionhandler.add_action(ban)

        await ctx.bot.pg_session.execute('INSERT INTO ban_data(user_id, guild_id, ban_reason, ban_date, ban_cmd_link, ban_author) VALUES($1, $2, $3, $4, $5, $6)'
                                         'ON CONFLICT (user_id, guild_id, ban_reason, ban_date) DO NOTHING', user_id, ctx.guild.id, flags.reason, date, ctx.message.jump_url, ctx.author.id)

        try:
            await ctx.guild.ban(discord.Object(id=user_id), reason=flags.reason, delete_message_seconds=int(delete_delta.total_seconds()))
            await self.bot.pg_session.execute('UPDATE ban_data SET tempban_resolved = $1 WHERE guild_id = $2 AND user_id = $3',
                                              True, ctx.guild.id, user_id)
            await ctx.invoke(self.citation_cmd, member=str(user_id),
                             reason=f'BANNED permanently for reason: {flags.reason}', silent=True)
        except discord.errors.NotFound:
            try:
                await self.bot.modactionhandler.remove_action(ban)
            except (IndexError, ValueError):
                pass
            return await ctx.send('No user with ID `{}` found.'.format(user_id))
        except discord.errors.Forbidden:
            try:
                await self.bot.modactionhandler.remove_action(ban)
            except (IndexError, ValueError):
                pass
            return await ctx.send('Invalid permissions to moderate target user.')

        await ctx.send('Goodbye <@{}>. <:salute:949392903380271204>'.format(user_id))

    @commands.command()
    @checks.admin_or_permissions(ban_members=True, aliases=['temp_ban'])
    async def tempban(self, ctx, user: discord.Member, time: str, *, flags: BanFlags):

        """Usage: <prefix>tempban <@user> <time (Xd/Xh/Xm)> [reason]
        Bans and then unbans a user after the specified <time>.
        The member will be PMed informing them of their temp ban but will not be offered an appeal."""

        date = ctx.message.created_at
        if not time:
            return await ctx.send('Please specify tempban duration in the form of `Xd/Xh/Xm` for days, hours, minutes respectively.')
        duration = durationparser(time)
        if not duration:
            return await ctx.send('Please specify tempban duration in the form of `Xd/Xh/Xm` for days, hours, minutes respectively.')
        end_time = date + duration

        ban = ModActionMetadata('ban', ctx.guild.id, user.id, ctx.author.id, flags.reason, date, ctx.message.jump_url, duration=duration)
        try:
            await self.bot.modactionhandler.add_action(ban)
        except ModActionCollision:
            await self.bot.modactionhandler.remove_action(ban)
            await self.bot.modactionhandler.add_action(ban)

        conn = self.bot.pg_session

        await conn.execute('INSERT INTO ban_data(user_id, guild_id, ban_reason, ban_date, temporary, end_time, ban_cmd_link, ban_author, tempban_resolved) VALUES('
                           '$1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT (user_id, guild_id, ban_date, ban_reason) DO NOTHING',
                           user.id, ctx.guild.id, flags.reason, date, True, end_time, ctx.message.jump_url, ctx.author.id, False)

        try:
            await user.send(f'You have been temporarily banned from {ctx.guild.name}. This ban expires on {discord.utils.format_dt(end_time)}.'
                            f'\nReason: `{flags.reason}`.')
        except (discord.errors.Forbidden, discord.errors.HTTPException):
            pass

        try:
            await ctx.guild.ban(user, reason=flags.reason, delete_message_days=0)
            await ctx.invoke(self.citation_cmd, member=str(user.id), reason=f'BANNED for {time} with reason: {flags.reason}', silent=True)
        except discord.errors.Forbidden:
            await self.bot.modactionhandler.remove_action(ban)
            await conn.execute('UPDATE ban_data SET tempban_resolved = $1 WHERE guild_id = $2 AND user_id = $3 AND ban_cmd_link = $4',
                               True, ctx.guild.id, user.id, ctx.message.jump_url)
            return await ctx.send('Invalid permissions to moderate target user.')

        await ctx.send(f'Goodbye `{user.name.replace("`", "")}`. <:salute:949392903380271204>')
        self.bot.tasks.append(self.bot.loop.create_task(self.ban_timer(ban)))

    @commands.command()
    @checks.admin_or_permissions(ban_members=True)
    async def unban(self, ctx, user: discord.User, *, reason: str = 'None'):

        """Usage: <prefix>unban <userid> [reason]
        Unbans the specified user with an optional reason and resolves all tempbans."""

        guild = ctx.guild
        date = ctx.message.created_at

        unban = ModActionMetadata('unban', ctx.guild.id, user.id, ctx.author.id, reason, date, ctx.message.jump_url)
        try:
            await self.bot.modactionhandler.add_action(unban)
        except ModActionCollision:
            await self.bot.modactionhandler.remove_action(unban)
            await self.bot.modactionhandler.add_action(unban)

        try:
            await guild.unban(user, reason=reason)
            await self.bot.pg_session.execute('UPDATE ban_data SET tempban_resolved = $1 WHERE guild_id = $2 AND user_id = $3',
                                              True, ctx.guild.id, user.id)
        except discord.errors.NotFound:
            try:
                await self.bot.modactionhandler.remove_action(unban)
            except (IndexError, ValueError):
                pass
            return await ctx.send(f'User is not banned in this server.')
        except discord.errors.Forbidden:
            try:
                try:
                    await self.bot.modactionhandler.remove_action(unban)
                except (IndexError, ValueError):
                    pass
            except (IndexError, ValueError):
                pass
            return await ctx.send('Invalid permissions to moderate target user.')

        await ctx.send(f'{getattr(user, "name", user.id)} unbanned.')

        try:
            await user.send('You have been unbanned from `{}`. Welcome back!'.format(guild.name))
        except discord.errors.Forbidden:
            pass

    @commands.command(help='Silences a channel for a duration. Say "resume" to cancel early.')
    @checks.admin_or_permissions(manage_channels=True)
    async def silence(self, ctx, time: int = 5):

        if ctx.channel.id in self.silence_channels:
            try:
                return await ctx.send('This channel is already silenced.')
            except discord.errors.Forbidden:
                return

        self.silence_channels.append(ctx.channel.id)
        time = max(0, min(60, time))

        time *= 60
        overwrites = ctx.channel.overwrites
        try:
            await ctx.send(f'Some peace and quiet... for {int(time/60)} minutes.')
        except discord.errors.Forbidden:
            pass

        if not overwrites:
            new_overwrite = discord.PermissionOverwrite(send_messages=False)
            await ctx.channel.set_permissions(discord.utils.get(ctx.guild.roles, name='@everyone'), overwrite=new_overwrite)

        else:
            for x in overwrites:
                target = x
                new_overwrite = deepcopy(overwrites[x])
                new_overwrite.send_messages = False
                await ctx.channel.set_permissions(target, overwrite=new_overwrite)

        def break_check(message):
            if not message.guild:
                return False
            return bool((message.channel.permissions_for(message.author)).manage_roles) & bool(
                message.content.lower() == 'resume')

        try:
            resume = await self.bot.wait_for('message', check=break_check, timeout=time)
        except asyncio.TimeoutError:
            resume = ctx.message

        for x in overwrites:
            target = x
            old_overwrite = overwrites[x]
            await ctx.channel.set_permissions(target, overwrite=old_overwrite)
        try:
            await resume.reply('Bring the noise.')
        except discord.errors.Forbidden:
            pass

        try:
            self.silence_channels.remove(ctx.channel.id)
        except ValueError:
            pass

    @commands.command(aliases=['mute'])
    @checks.mod_or_permissions(moderate_members=True)
    async def timeout(self, ctx: commands.Context, member: discord.Member, time: Union[int, str], *, reason: str = 'None'):

        """Usage: <prefix>timeout <member> <minutes OR Xd/Xh/Xm> [reason]
        Times <member> out for the duration specified by <minutes OR Xd/Xh/Xm>."""

        if isinstance(time, str):
            _duration = durationparser(time)
            if not _duration:
                return await ctx.send('Invalid timeout duration supplied, use `Xd/Xh/Xm` for days/hours/minutes respectively.')
            duration_mins = _duration.total_seconds() // 60
        elif isinstance(time, int):
            duration_mins = time
        else:
            return await ctx.send('Invalid timeout duration supplied.')

        duration = datetime.timedelta(minutes=max(0, min(40320, duration_mins)))
        date = ctx.message.created_at

        timeout = ModActionMetadata('timeout', ctx.guild.id, member.id, ctx.author.id, reason, date, ctx.message.jump_url, duration=duration)
        try:
            await self.bot.modactionhandler.add_action(timeout)
        except ModActionCollision:
            await self.bot.modactionhandler.remove_action(timeout)
            await self.bot.modactionhandler.add_action(timeout)

        try:
            if duration.total_seconds() > 0:
                await member.timeout(duration, reason=reason)
                await ctx.invoke(self.citation_cmd, member=str(member.id), reason=f'TIMED OUT for {duration.total_seconds() // 60} minutes with reason: {reason}', silent=True)
            else:
                await member.edit(timed_out_until=None, reason=reason)
                try:
                    await member.send(f'Your timeout in `{ctx.guild}` has been removed. Reason: `{reason}`')
                except (discord.errors.Forbidden, discord.errors.HTTPException):
                    pass
                return await ctx.send(f'Timeout removed for `{member.name}`.')
        except discord.Forbidden:
            try:
                await self.bot.modactionhandler.remove_action(timeout)
            except (IndexError, ValueError):
                pass
            return await ctx.send("Invalid permissions to time out user.")

        try:
            await member.send(f'You have been timed out in `{ctx.guild}` until '
                              f'{discord.utils.format_dt(date + duration)}. Reason: `{reason}`.')
        except (discord.errors.Forbidden, discord.errors.HTTPException):
            pass

        await ctx.send(f'`{member.name}` timed out until {discord.utils.format_dt(date + duration)}')

    @commands.command()
    @checks.admin_or_permissions()
    async def set_welcome(self, ctx, *, message: str=None):

        """Sets a welcome message in the current channel.
        "$usermention$" will be replaced by a mention of the joining user.
        "$username$" will be replaced by the display name of the joining user.
        Call without arguments to see the current welcome message in the channel.
        You will be given the option to remove the current welcome message as well."""

        gid, cid = ctx.guild.id, ctx.channel.id
        conn = self.bot.pg_session
        prev = self.welcome_channels.get(gid, {}).get(cid)

        if not message:
            if not prev:
                return await ctx.send('No welcome message set in this channel.')

            msg = await ctx.send(f'Current welcome message is:```\n{self.welcome_channels[gid][cid]}```React with ❌ to remove.')
            await msg.add_reaction('❌')

            def chk(reaction, user):
                return (reaction.emoji == '❌') & (user.id == ctx.message.author.id)

            try:
                await self.bot.wait_for('reaction_add', check=chk, timeout=120)
            except asyncio.TimeoutError:
                return

            self.welcome_channels[gid].pop(cid)
            await conn.execute('DELETE FROM welcomes WHERE guild_id = $1 AND channel_id = $2', gid, cid)
            return await ctx.send('No longer welcoming in this channel.')

        if gid not in self.welcome_channels:
            self.welcome_channels.update({gid: {}})

        self.welcome_channels[gid].update({cid: message})
        await conn.execute('INSERT INTO welcomes(guild_id, channel_id, message) VALUES($1, $2, $3) ON CONFLICT (guild_id, channel_id) '
                           'DO UPDATE SET message = $3 WHERE welcomes.guild_id = $1 AND welcomes.channel_id = $2',
                           gid, cid, message)

        return await ctx.send(f'Welcome message in this channel is now:```\n{message}```')

    @commands.command(help='Add/remove server bot prefixes or view current.')
    @commands.guild_only()
    @checks.admin_or_permissions()
    async def set_prefix(self, ctx, *, prefix: str = None):

        await self.bot.wait_until_ready()

        if prefix:
            if len(prefix) > 50:
                return await ctx.send('Prefix too long.')
            if prefix.endswith(' "'):
                prefix = prefix[1:-1]

        conn = self.bot.pg_session
        row = await conn.fetch('SELECT prefixes FROM guild_prefs WHERE guild_prefs.guild_id = $1', ctx.guild.id)

        if not row:
            self.bot.guild_prefs.update({ctx.guild.id: {}})
            if not prefix:
                return await ctx.send('No custom prefixes currently.')
            new_pres = [prefix]

        else:
            row = row[0]
            if not prefix:
                embed = discord.Embed()
                val = await self.bot.get_prefix(ctx.message)
                embed.add_field(name='Current Prefixes:', value=val)
                return await ctx.send(embed=embed)

            new_pres = row['prefixes']
            if not row['prefixes']:
                new_pres = [prefix]
            elif prefix in row['prefixes']:
                if len(new_pres) <= 1:
                    return await ctx.send('You cannot remove all prefixes. It\'s for your own good.')
                new_pres.remove(prefix)
            else:
                new_pres.append(prefix)

        if len(str(new_pres)) > 1024:
            return await ctx.send("Total length of all prefixes too large.")

        await conn.execute('INSERT INTO guild_prefs(guild_id, prefixes) VALUES($1, $2) ON CONFLICT (guild_id) '
                           'DO UPDATE SET prefixes = $2 WHERE guild_prefs.guild_id = $1', ctx.guild.id, new_pres)

        if not self.bot.guild_prefs.get(ctx.guild.id):
            self.bot.guild_prefs.update({ctx.guild.id: {}})
        self.bot.guild_prefs[ctx.guild.id].update({'prefixes': new_pres})
        embed = discord.Embed()
        val = await self.bot.get_prefix(ctx.message)
        embed.add_field(name='Current Prefixes:', value=val)
        return await ctx.send(embed=embed)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def ban_sync(self, ctx):

        guild = ctx.guild
        bans = [ban async for ban in guild.bans(limit=None)]
        date = datetime.datetime.now(tz=datetime.timezone.utc)

        for ban in bans:
            user = ban.user
            reason = ban.reason
            await self.bot.pg_session.execute('''INSERT INTO ban_data(user_id, guild_id, ban_reason, ban_date, ban_cmd_link)
                             VALUES($1, $2, $3, $4, $5) ON CONFLICT (user_id, guild_id, ban_date, ban_reason) DO NOTHING''',
                                              user.id, guild.id, reason, date, 'Synced ban.')

        await ctx.send('Bans synced for {}.'.format(guild.name))

    @commands.command(help='Add/remove a word or phrase to the server filter.')
    @checks.admin_or_permissions()
    async def filter(self, ctx, *, phrase=None):

        conn = self.bot.pg_session

        words = (await conn.fetchrow('SELECT filter_words FROM guild_prefs WHERE guild_id = $1', ctx.guild.id))['filter_words']

        if not phrase:
            return await ctx.send('Current filter list: `{}`.\nSpecify new or old phrase to update.'.format(words))

        phrase = phrase.lower()

        if words:
            if phrase in words:
                words.remove(phrase)
                await conn.execute('UPDATE guild_prefs SET filter_words = $1 WHERE guild_prefs.guild_id = $2', words, ctx.guild.id)

            else:
                words.append(phrase)
                await conn.execute('INSERT INTO guild_prefs(guild_id, filter_words) VALUES($1, $2) '
                               'ON CONFLICT (guild_id) DO UPDATE SET filter_words = $3 WHERE guild_prefs.guild_id = $1', ctx.guild.id, [phrase], words)

        else:
            words = [phrase]
            await conn.execute('INSERT INTO guild_prefs(guild_id, filter_words) VALUES($1, $2) '
                               'ON CONFLICT (guild_id) DO UPDATE SET filter_words = $2 WHERE guild_prefs.guild_id = $1', ctx.guild.id, [phrase])

        self.bot.filter[ctx.guild.id] = words
        await ctx.send('Server filter is now `{}`.'.format(words))

    @commands.command(aliases=['rapsheet'])
    async def citation(self, ctx, member=None, *, reason: str = None, silent: bool = False):

        """Usage: <prefix>citation <@user> [reason OR remove x OR clear]
           Add citation to <@user> with timestamp and [reason].
           Call with only <@user> argument to view current citations.
           Call with <prefix>citation <user> edit <x> <new text> to edit an existing citation.
           Call with <prefix>citation <user> remove <x> to remove a specific citation by index.
           Owner can remove any citation, administrators can remove any non-admin or their own citations.
           Everyone else can only remove their own citations."""

        # TODO clean parsing logic up, maybe use some flag hacks or subcommands?
        # improvements to Citation interface could be good too

        if not member:
            uid = ctx.author.id
            name = ctx.author.name
            reason = None
            silent = False

        elif await checks.check_guild_permissions(ctx, {'administrator': True, 'moderate_members': True}, check=any):
            try:
                member = await discord.ext.commands.converter.MemberConverter().convert(ctx, member)
                uid = member.id
                name = member.name
            except discord.ext.commands.errors.BadArgument:
                try:
                    uid = int(member)
                    name = str(uid)
                except ValueError:
                    return await ctx.send('Could not convert argument to member.')
        else:
            return

        gid = ctx.guild.id
        authorid = ctx.author.id
        citation_link = ctx.message.jump_url

        if not reason:

            prevs = self.citations.get(gid, {}).get(uid, [])

        elif reason.lower() == 'clear':

            prevs = self.citations.get(gid, {}).get(uid, [])
            news = [c for c in prevs if not self.citation_check(ctx, c)]
            to_remove = [c for c in prevs if c not in news]
            amount = len(prevs) - len(news)
            if amount == 0:
                return await ctx.send('No valid citations to clear.')

            def sanity_check_check(message):

                return (message.author.id == ctx.author.id) & (message.channel.id == ctx.channel.id)

            confirmed = await self.sanity_check(ctx, sanity_check_check,
                                                f'Are you sure you want to clear {amount} citations? This action is not reversible. Type "yes" or "y" to confirm.',
                                                'Aborting citation clear.')
            if not confirmed:
                return

            for cit in to_remove:
                await self.bot.pg_session.execute('DELETE FROM citations WHERE guild_id = $1 AND member_id = $2 '
                                                  'AND author_id = $3 AND citation_time = $4 AND citation_text = $5',
                                                  gid, uid, cit.author, cit.timestamp, cit.reason)
            self.citations.get(gid, {})[uid] = news
            return await ctx.send(f'Your citations for `{name}` cleared.')

        elif reason.lower().split()[0] in ['remove', 'delete']:

            try:
                num = int(reason.lower().split()[-1]) - 1
                if num < 0:
                    return await ctx.send('Citation index must be at least 1.')
                prev = self.citations.get(gid, {})[uid]
                to_remove = prev[num]
                if not self.citation_check(ctx, to_remove):
                    return await ctx.send('Insufficient permissions to remove target citation.')
                removed = prev.pop(num)
            except (ValueError, IndexError):
                return await ctx.send('Invalid citation index.')
            except KeyError:
                return await ctx.send(f'User `{name.replace("`", "")}` has no citations.')

            if gid not in self.citations:
                self.citations.update({gid: {}})

            g_prev = self.citations.get(gid, {})
            g_prev.update({uid: prev})

            await self.bot.pg_session.execute('DELETE FROM citations WHERE guild_id = $1 AND member_id = $2 '
                                              'AND citation_text = $3 AND citation_time = $4 AND author_id = $5',
                                              gid, uid, removed.reason, removed.timestamp, removed.author)

            embed = discord.Embed(title=f'Citation #{num + 1} removed by `{ctx.author.name}`:')
            embed.add_field(name=self.zws, value=f'[Removed Citation]({removed.command_link}) on {discord.utils.format_dt(removed.timestamp)}```\n{removed.reason}```', inline=False)
            return await ctx.send(embed=embed)

        elif reason.lower().split()[0] == 'edit':

            try:
                num = int(reason.lower().split()[1]) - 1
                if num < 0:
                    return await ctx.send('Citation index must be at least 1.')
                prev = self.citations.get(gid, {})[uid]
                to_edit = prev[num]
                if not self.citation_check(ctx, to_edit):
                    return await ctx.send('Insufficient permissions to edit target citation.')
            except (ValueError, IndexError):
                return await ctx.send('Invalid citation index.')
            except KeyError:
                return await ctx.send(f'User `{name.replace("`", "")}` has no citations.')

            if gid not in self.citations:
                self.citations.update({gid: {}})

            new_reason = ' '.join(reason.lower().split()[2:]).replace('`', '')
            if not new_reason:
                return await ctx.send(f'Must supply a new citation message.')
            new_reason = 'EDITED: ' + new_reason
            prev_text = to_edit.reason
            prev_link = to_edit.command_link
            prev_timestamp = to_edit.timestamp
            prev_author = to_edit.author

            await self.bot.pg_session.execute('UPDATE citations SET citation_text = $1, author_id = $2, citation_link = $3, '
                                              'citation_time = $4 WHERE citation_time = $5 AND author_id = $6 '
                                              'AND member_id = $7 AND guild_id = $8 AND citation_text = $9',
                                              new_reason, ctx.author.id, ctx.message.jump_url, ctx.message.created_at,
                                              prev_timestamp, prev_author, uid, gid, prev_text)
            to_edit.reason = new_reason
            to_edit.author = ctx.author.id
            to_edit.command_link = ctx.message.jump_url
            to_edit.timestamp = ctx.message.created_at

            embed = discord.Embed(title=f'Citation #{num + 1} updated by `{ctx.author.name}`:')
            embed.add_field(name=self.zws, value=f'[Old Citation]({prev_link}) on {discord.utils.format_dt(prev_timestamp)}```\n{prev_text}```', inline=False)
            embed.add_field(name=f'', value=f'[New Citation]({to_edit.command_link}) on {discord.utils.format_dt(to_edit.timestamp)}```\n{new_reason}```', inline=False)
            return await ctx.send(embed=embed)

        else:
            reason = reason.replace('`', '')
            citation = Citation(ctx.message.created_at, reason, authorid, citation_link)

            if gid not in self.citations:
                self.citations.update({gid: {}})

            prev = self.citations.get(gid, {}).get(uid, [])
            prev.append(citation)
            self.citations.get(gid, {}).update({uid: prev})

            prevs = self.citations.get(gid, {}).get(uid, [])

            await self.bot.pg_session.execute(
                'INSERT INTO citations (guild_id, member_id, author_id, citation_time, citation_text, citation_link) '
                'VALUES ($1, $2, $3, $4, $5, $6)', gid, uid, authorid, citation.timestamp, reason, citation_link)
            if silent:
                citation_link_text = f'{f"[Citation #{len(prevs)}]({citation_link})" if citation_link else f"Citation #{len(prevs) - 1}"}'
                embed = CustomEmbed(title='Auto-Citation',
                                    description=f'{citation_link_text} Added for `{name}`:\n```\n{len(prevs)}: {reason}```')
                return await ctx.send(embed=embed)

        if not prevs:
            return await ctx.send(f'No citations for user `{name}`.')

        embeds = []

        chunk_size = 5
        for h, chunk in enumerate(list(chunk_list(prevs, chunk_size))):

            start_citation = max(1, 1 + chunk_size * h)
            end_citation = min(len(prevs), chunk_size * (h + 1))
            embed = CustomEmbed(title=f'Citations {start_citation} to {end_citation}/{len(prevs)} for User `{name}`:')
            field_strings = ['']

            for i, cit in enumerate(chunk):
                cur_str = field_strings[-1]
                i_citation_link = cit.command_link
                citation_link_text = f'{f"[Citation #{start_citation + i}]({i_citation_link})" if i_citation_link else f"Citation #{start_citation + i}"}'
                str_to_add = (f'{citation_link_text} on {discord.utils.format_dt(cit.timestamp)}:\n'
                              f'Author: <@{cit.author}> ```\n{cit.reason}```\n')
                if len(cur_str + str_to_add) > embed.value_limit:
                    field_strings.append(str_to_add)
                else:
                    field_strings[-1] += str_to_add

            embed.add_field(name=self.zws, value=field_strings, inline=False)
            embeds.append(embed)

        if not embeds:
            return await ctx.send(f'No citations for user `{name}`.')
        view = ButtonPaginator(caller=ctx.author.id, page=len(embeds) - 1, embed_pages=embeds)
        await ctx.send(embed=embeds[-1], view=view)
        await view.wait()

    @commands.command()
    @checks.admin_or_permissions(manage_messages=True)
    async def giveaway(self, ctx, *, args):

        """Start a giveaway that randomly picks from users who react.
           This command is available only to admins or those with the Manage Messages permission.

           Usage:
           This command is flag-based and has only one required argument: "item".
           Flags must be followed with a colon and an argument, and are comma-separated.
           Below is a list of flags and what they affect.

               item: the item being given away
               timer: an integer for duration of the giveaway in hours.
                      Defaults to 24, minimum of 1 and max of 168
               rolelock: a role ID or case-sensitive name
                         which will limit the winners to
                         members with the given role
               ping: a role ID or case-sensitive name
                     which will be pinged with the initial message
               winners: an integer for the number of entrants who can win.
                        Will pick everyone if the number of entrants
                        is lower that the specified number of winners
               via: a username/user ID/mention of the server member who is
                    giving away the item(s). Defaults to command author

           Example with all flags:
               "<prefix> giveaway item: World Domination, timer: 48, rolelock: Bot, ping: Giveaways, winners: 1, via: Theroxenes"
           """

        flags = await self.get_flags_giveaway(ctx, args)
        newln = '\n'

        if not flags:
            return

        await ctx.message.add_reaction('✅')

        if flags.get('ping'):
            try:
                await flags.get('ping').edit(mentionable=True)
            except discord.errors.Forbidden:
                pass

        formatted_time = timestampformat(datetime.datetime.now() + datetime.timedelta(hours=flags.get('timer')))

        entry = await ctx.send(f'{flags.get("ping").mention + newln if flags.get("ping") else ""}'
                               f'{flags.get("via", ctx.author).mention} has started a giveaway for `{flags.get("item")}`. This giveaway '
                               f'{("is locked to the `" + flags.get("rolelock").name + "` role, and ") if flags.get("rolelock") else ""}'
                               f'expires {formatted_time}. React with ✅ to enter.')

        if flags.get('ping'):
            try:
                await flags.get('ping').edit(mentionable=False)
            except discord.errors.Forbidden:
                pass

        await entry.add_reaction('✅')

        await asyncio.sleep(flags['timer'] * 3600)
        await self.roll_giveaway(ctx, args=args, target=entry.id)

    @commands.command()
    @checks.admin_or_permissions(manage_messages=True)
    async def roll(self, ctx, msg_id: int, *, args: str):

        await self.roll_giveaway(ctx, args=args, target=msg_id)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def eval(self, ctx, *, eval_str):
        env = {
            'bot': self.bot,
            'ctx': ctx,
            'channel': ctx.channel,
            'author': ctx.author,
            'guild': ctx.guild,
            'message': ctx.message,
            '_': self._last_result
        }

        env.update(globals())
        if eval_str.startswith('```py\n') & eval_str.endswith('```'):
            body = '\n'.join(eval_str.splitlines()[1:])[:-3]
        elif eval_str.startswith('```') & eval_str.endswith('```'):
            body = eval_str[3:-3]
        elif eval_str.startswith('`') & eval_str.endswith('`'):
            body = eval_str[1:-1]
        else:
            body = eval_str

        stdout = io.StringIO()

        to_compile = f'async def func():\n{textwrap.indent(body, "  ")}'
        try:
            exec(to_compile, env)
        except Exception as e:
            for x in paginate(f'{e.__class__.__name__}: {e}\n'):
                await ctx.send(x)
            return

        func = env['func']
        try:
            with redirect_stdout(stdout):
                ret = await func()
        except Exception as e:
            value = stdout.getvalue()
            for x in paginate(f'{value}{traceback.format_exc()}\n'):
                await ctx.send(x)
        else:
            value = stdout.getvalue()

            if ret is None:
                if value:
                    for x in paginate(f'{value}\n'):
                        await ctx.send(x)
            else:
                self._last_result = ret
                for x in paginate(f'{value}{ret}\n'):
                    await ctx.send(x)

    @commands.command(brief='Toggles command on/off in current channel.',
                      help='Toggles command on/off in current channel. Requires owner/admin/manage channel permission.')
    @checks.admin_or_permissions(manage_channels=True)
    async def togglecommand(self, ctx, command_name: str = None):

        await self.bot.wait_until_ready()

        if not command_name:
            disabled = []
            if ctx.guild.id in self.bot.toggled_commands:
                for cmd in [x.name for x in ctx.bot.commands]:
                    if cmd in self.bot.toggled_commands[ctx.guild.id]:
                        if ctx.channel.id in self.bot.toggled_commands[ctx.guild.id][cmd]:
                            disabled.append(cmd)
            nwln = '\n'
            return await ctx.send(f'Please specify a command to toggle. Use {ctx.prefix}help to see available commands.'
                                  f'\nCurrently disabled commands:```\n{nwln.join(disabled) or "None"}```')

        if command_name in ['togglecommand', 'help']:
            return await ctx.send('You cannot disable this command. It\'s for your own good.')

        if command_name not in [x.name for x in ctx.bot.commands]:
            return await ctx.send(f'Command not found. See `{ctx.prefix}help` for available commands.')

        else:
            cogdict = {x.name: x.cog_name for x in ctx.bot.commands}
            if cogdict[command_name] == 'PrivateCommands':
                return await ctx.send(f'Command not found. See `{ctx.prefix}help` for available commands.')

        conn = ctx.bot.pg_session
        row = await conn.fetch('SELECT * FROM guild_prefs WHERE guild_id = $1', ctx.guild.id)
        db_commandname = f'{command_name}_command'
        on_or_off = "off"
        if row:
            if db_commandname in row[0].keys():
                old = row[0][db_commandname]
                if not bool(old):
                    old = []
                if ctx.channel.id in old:
                    old.remove(ctx.channel.id)
                    on_or_off = "on"
                else:
                    old.append(ctx.channel.id)
                await conn.execute(f'UPDATE guild_prefs SET {db_commandname} = $1 WHERE guild_prefs.guild_id = $2', old,
                                   ctx.guild.id)
                if ctx.guild.id not in self.bot.toggled_commands:
                    self.bot.toggled_commands.update({ctx.guild.id: {}})
                self.bot.toggled_commands[ctx.guild.id].update({command_name: old})
                return await ctx.send(f'Command `{command_name}` toggled {on_or_off}.')

        await conn.execute(
            f'ALTER TABLE guild_prefs ADD COLUMN IF NOT EXISTS {db_commandname} bigint[]')  # , command_name)
        await conn.execute(
            f'INSERT INTO guild_prefs(guild_id, {db_commandname}) VALUES($1, $2) ON CONFLICT (guild_id) DO UPDATE '
            f'SET {db_commandname} = $2 WHERE guild_prefs.guild_id = $1', ctx.guild.id, [ctx.channel.id])

        if ctx.guild.id not in self.bot.toggled_commands:
            self.bot.toggled_commands.update({ctx.guild.id: {}})
        self.bot.toggled_commands[ctx.guild.id].update({command_name: [ctx.channel.id]})
        await ctx.send(f'Command `{command_name}` toggled {on_or_off}.')

    @commands.command()
    @checks.admin_or_permissions(create_expressions=True)
    async def yoink(self, ctx, message: discord.Message, *, name: str):

        """Usage: <prefix>yoink <message ID/link> <name>
        Yoinks the first sticker or emoji in the target <message ID/link> and adds it to the server with <name>."""

        if not message:
            return await ctx.send('No target message, try again with a message ID or jump link.')

        is_sticker = True

        if message.stickers:
            url = message.stickers[0].url
        else:
            is_sticker = False
            emoji = self.emojiregex.search(message.content)
            if not emoji:
                return await ctx.send(f'No emojis to yoink from target message `{message}`.')

            emoji_id = emoji.group('id')
            if emoji.group('animated'):
                url = f'https://cdn.discordapp.com/emojis/{emoji_id}.gif'
            else:
                url = f'https://cdn.discordapp.com/emojis/{emoji_id}.png'

        fp_out = io.BytesIO()
        async with aiohttp.ClientSession() as sesh:
            async with sesh.get(url) as resp:
                if resp.status == 200:
                    fp_out.write(await resp.read())
                    fp_out.seek(0)

        try:
            if is_sticker:
                name = name.strip()
                await ctx.guild.create_sticker(name=name, file=discord.File(fp_out), emoji='robot', description="Yoinked!")
            else:
                name = re.sub(r'\s', '', name)
                await ctx.guild.create_custom_emoji(name=name, image=fp_out.read())

            await ctx.send(f'{"Sticker" if is_sticker else "Emoji"} `{name}` successfully yoinked.')
        except Exception as e:
            return await ctx.send(f'Error creating {"sticker" if is_sticker else "emoji"}: {e}')

    @commands.Cog.listener()
    async def on_member_join(self, member):

        guild = member.guild
        old = self.new_users.get(guild.id, [])
        old.append(member.id)
        self.new_users.update({guild.id: old})

        if await self.appeal_channel_check(guild):
            if await self.is_banned_check(member):

                name = 'ban-appeal-{}'.format(member.id)

                if 'Active ban appeals' not in [cat.name for cat in guild.categories]:
                    await guild.create_category(name='Active ban appeals')

                category = discord.utils.get(guild.categories, name='Active ban appeals')
                exists = discord.utils.get(guild.channels, name=name)

                if exists:
                    await exists.edit(category=category)
                    await exists.set_permissions(member, overwrite=
                    discord.PermissionOverwrite(read_messages=True, send_messages=True, attach_files=True,
                                                read_message_history=True, embed_links=True)
                                                 )
                    chan = exists

                else:
                    chan = await guild.create_text_channel(name=name, category=category, overwrites={
                        member: discord.PermissionOverwrite(read_messages=True, send_messages=True, attach_files=True,
                                                            read_message_history=True, embed_links=True)
                    })

                await chan.send('{}, your appeal process has begun.'.format(member.mention))

        mention = member.mention
        name = member.display_name

        if re.match(r'.*discord\.gg/.*', member.name):

            try:
                await member.edit(nick=f'Bad Name #{self.bot.badboycount}')
                self.bot.badboycount += 1
            except discord.errors.Forbidden:
                return

            name = f'Bad Username: ID {member.id}'

        welcome_chans = self.welcome_channels.get(guild.id, {})
        for chan in welcome_chans:
            chan = guild.get_channel(chan)
            if not chan:
                continue

            await chan.send(welcome_chans[chan.id].replace('$usermention$', mention).replace('$username$', f'`{escape_nick(name)}`')[:2000])

    @commands.Cog.listener()
    async def on_member_remove(self, member):

        guild = member.guild

        if await self.appeal_channel_check(guild):
            name = 'ban-appeal-{}'.format(member.id)
            if name in [chan.name for chan in guild.channels]:
                chan = discord.utils.get(guild.channels, name=name)
                await chan.send('Appeal resolved. Closing.')

                if 'Resolved ban appeals' not in [cat.name for cat in guild.categories]:
                    await guild.create_category(name='Resolved ban appeals')

                await chan.edit(category=discord.utils.get(guild.categories, name='Resolved ban appeals'))

    @commands.Cog.listener()
    async def on_command_error(self, ctx, exc):

        # Formats exception for pretty log printing
        format_exc = '{}.{}: {}\n{}\n'.format(type(exc).__module__, type(exc).__qualname__, exc,
                                              ''.join(traceback.format_exception(type(exc), exc, exc.__traceback__))
                                              if exc.__traceback__ is not None else '')

        # Add some additional info (original message, guild + channel IDs)
        if ctx.guild:
            gid = ctx.guild.id
        else:
            gid = 'PM'

        embed = discord.Embed(title='Error when invoking command:')
        embed.add_field(name='Guild/Channel IDs:', value=f'{gid} / {ctx.channel.id}')
        embed.add_field(name='Context content:', value=ctx.message.content)
        embed.add_field(name='Attachments:', value="".join([x.proxy_url for x in ctx.message.attachments]) or None)
        if ctx.guild:
            embed.set_footer(text=f'Shard ID: {ctx.guild.shard_id}')

        if isinstance(exc, (discord.ext.commands.errors.CheckFailure, discord.ext.commands.errors.CommandNotFound,
                            discord.ext.commands.errors.CommandOnCooldown)):
            pass

        elif isinstance(exc, discord.ext.commands.errors.BadArgument) | isinstance(exc,
                                                                                   discord.ext.commands.MissingRequiredArgument):
            await ctx.send(f'Error in parsing command argument(s):\n{exc}')
            #await ctx.send_help(ctx.command)

        else:
            logging.error('Ignoring exception in command {}:'.format(ctx.command))
            logging.error('{}.{}: {}\n{}\n'.format(type(exc).__module__, type(exc).__qualname__, exc,
                                                   ''.join(
                                                       traceback.format_exception(type(exc), exc, exc.__traceback__))
                                                   if exc.__traceback__ is not None else ''))

            # Retrieve bug_log channel
            test_chan = self.bot.get_channel(356477444355522560)
            # Paginate and send
            await test_chan.send(embed=embed)
            for x in self.paginate(format_exc):
                await test_chan.send(x)

            try:
                await ctx.send('Something... has gone terribly wrong <@183410776462196736>.')
            except discord.Forbidden:
                pass


async def setup(bot):
    await bot.add_cog(Moderation(bot))
