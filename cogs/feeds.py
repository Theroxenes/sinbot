import datetime
import aiohttp
import asyncpg
import discord

from discord.ext import commands, tasks
from typing import Union, List, Set
from collections.abc import Coroutine, Callable

from .ext import checks

delay = 600


class NewsItem:

    __slots__ = ['content', 'uuid', 'urls', 'html', 'content_formatter', 'embed_formatter']

    def __init__(self, content: str, uuid: Union[str, int], urls: List[str]=[],
                 content_formatter: Callable[[], Coroutine[None, None, Union[None, str]]]=None,
                 embed_formatter: Callable[[], Coroutine[None, None, Union[None, discord.Embed]]]=None):

        self.content = content
        self.urls = urls
        self.uuid = str(uuid)

        if content_formatter:
            self.content_formatter = content_formatter
        else:
            self.content_formatter = self._default_content_formatter

        if embed_formatter:
            self.embed_formatter = embed_formatter
        else:
            self.embed_formatter = self._default_embed_formatter

    async def format_content(self) -> Union[None, str]:

        """Calls the constructed content formatter during feed dispatching."""

        return await self.content_formatter()

    async def _default_content_formatter(self) -> str:

        """Default content formatter, pass another one during NewsItem construnction to override.
        Formatter must take zero arguments (may revisit this design choice later)."""

        output = '\n'.join([self.content, '\n'.join(self.urls)])
        if len(output) > 2000:

            raise ValueError

        return output

    async def format_embed(self) -> Union[None, discord.Embed]:

        """Optional Embed formatter, default sends no Embed."""

        return await self._default_embed_formatter()

    async def _default_embed_formatter(self) -> None:

        return None

    def __str__(self):

        return '\n'.join([self.content, '\n'.join(self.urls)])

    def __repr__(self):

        return str(self)


class NewsItemList:

    __slots__ = ['fetcher', 'newsitems', 'last_sent', 'header_map', 'publish_map', 'dispatched_uuids', 'cached_uuids']

    def __init__(self, fetcher: Callable[[], Coroutine[None, None, Union[NewsItem, List[NewsItem]]]]):

        self.newsitems: List[NewsItem] = []
        self.cached_uuids: Set[str] = set()

        self.header_map = {}
        self.publish_map = {}
        self.dispatched_uuids = {}

        self.fetcher = fetcher
        self.last_sent = None

    async def send_last(self, destination: discord.abc.Messageable) -> Union[None, discord.Message]:

        """Sends the most recent NewsItem to the destination."""

        if not self.last_sent:
            return await destination.send(f'No cached items for this feed.')
        return await destination.send(content=await self.last_sent.format_content(), embed=await self.last_sent.format_embed())

    async def dispatch_items(self, destination: discord.abc.Messageable, append_mode=False) -> List[discord.Message]:

        """Main NewsItem dispatching method. Handles sending, caching UUIDS, etc."""

        # Fetch any new items with the proper append mode
        await self.fetch(append_mode=append_mode)
        sent_messages = []

        # Iterate through news items and send them to the provided destination
        for item in self.newsitems:

            # Set the header for injecting during send later
            item_header = self.header_map.get(destination.id, None)

            # Initiate dispatched_uuids dict for this destination if it doesn't exist yet
            if destination.id not in self.dispatched_uuids:
                self.dispatched_uuids.update({destination.id: []})

            # Check if this item has been sent to this destination, if not, send it
            if item.uuid not in self.dispatched_uuids.get(destination.id, set()):

                try:
                    # Actually send the message and store it for return later
                    nwln = '\n'
                    sent_msg = await destination.send(content=f'{f"{item_header}{nwln}" if item_header else ""}{await item.format_content()}', embed=await item.format_embed())
                    sent_messages.append(sent_msg)

                    # Check if publish flag is set for this destination, if so try to publish the sent message
                    if self.publish_map.get(destination.id, False):
                        try:
                            await sent_msg.publish()
                        except (discord.Forbidden, discord.HTTPException):
                            pass

                    # Check if this NewsItem has been seen globally before, if not cache it as the last_sent
                    # This check prevents deferred sends to a specific destination from overriding the global last_sent
                    # i.e. if a channel is temporarily unreachable a later dispatch could rever the last_sent
                    if item.uuid not in self.cached_uuids:
                        self.last_sent = item

                    # Now update the destination-specific and global UUID caches
                    self.dispatched_uuids.get(destination.id).append(item.uuid)
                    self.cached_uuids.update({item.uuid})

                except (discord.Forbidden, discord.HTTPException) as e:
                    pass

        return sent_messages

    async def fetch(self, append_mode=False) -> List[NewsItem]:

        """Handles fetching NewsItem(s) via a coroutine supplied at instantiation."""

        # Get the NewsItem(s)
        fetched = await self.fetcher()

        # 2 modes are available: append or no-append. In append mode items are added one-by-one
        # In no-append the entire NewsItem list is supplied at once by the fetcher
        if append_mode and fetched:
            self.newsitems.append(fetched)
            return self.newsitems

        elif fetched:
            self.newsitems = fetched
            return self.newsitems

        return []


class Feed:

    __slots__ = ['name', 'source', 'recovered', 'destinations', 'conn', 'append_mode']

    def __init__(self, name: str, source: Callable[[], Coroutine[None, None, Union[NewsItem, List[NewsItem]]]], conn: asyncpg.Pool, append_mode: bool=False):

        self.name = name
        self.source = NewsItemList(source)
        self.conn = conn
        self.append_mode = append_mode
        self.destinations: List[discord.abc.Messageable] = []
        self.recovered = False

    async def setup(self, bot: commands.Bot):

        """Setup method to pull database data in and initiate the Feed's NewsItemList."""

        rows = await self.conn.fetch('SELECT * FROM feeds WHERE feeds.name = $1', self.name)
        for row in rows:

            destination_id = row['destination']

            # DM channels are stored as user IDs because of how discord works
            # If the bot can't get the channel it won't load the data in
            dest = bot.get_channel(destination_id) or bot.get_user(destination_id)

            if dest:
                self.destinations.append(dest)

                self.source.header_map.update({destination_id: row['header']})
                self.source.publish_map.update({destination_id: row['publish']})
                self.source.dispatched_uuids.update({destination_id: row['dispatched']})
                self.source.cached_uuids.update(set(row['dispatched']))

        self.recovered = True

    async def toggle(self, destination: discord.abc.Messageable, header:str ='', publish: bool=False, forceoff: bool=False) -> str:

        """Method to handle database logic of toggling/updating feed subscriptions."""

        # Optional forceoff param forces the subscription to be disabled, used by unsubscribe command
        # Otherwise, if the destination is already subscribed and the optional <header> and <publish> params are
        #   either not specified or not present, unsubscribe
        if forceoff or (destination in self.destinations and ((self.source.header_map.get(destination.id, '') == header\
                and self.source.publish_map.get(destination.id, False) == publish) or (not header and not publish))):
            self.destinations.remove(destination)
            state = False

        # In all other cases, add the destination if it is not present
        else:
            if destination not in self.destinations:
                self.destinations.append(destination)
            state = True

        # Formatted string to pass back to the subscribe/unsubscribe command later
        toggled = f'{"on" if state else "off"}{f" with header `{header}`" if header and state else ""}{f", publication set to `{publish}`" if state else ""}'

        if state:
            # If we turned on/edited a subscription, update the headers and initialize the destination-specific cache
            #   with the global UUID cache values. This prevents old NewsItems from being dispatched to new destinations
            self.source.header_map.update({destination.id: header})
            self.source.publish_map.update({destination.id: publish})
            self.source.dispatched_uuids.update({destination.id: list(self.source.cached_uuids)})

            # Then write all that to the database for restart recovery
            await self.conn.execute('INSERT INTO feeds (name, destination, header, publish, dispatched) '
                                    'VALUES ($1, $2, $3, $4, $5) ON CONFLICT (name, destination) DO UPDATE SET '
                                    'header = $3, publish = $4, dispatched = $5 WHERE feeds.name = $1 AND feeds.destination = $2',
                                    self.name, destination.id, self.source.header_map.get(destination.id, ''),
                                    self.source.publish_map.get(destination.id, False),
                                    self.source.dispatched_uuids.get(destination.id, []))
        else:
            # If unsubscribed clear the database entry for the destination and pop it out of the header/publish mappings
            await self.conn.execute('DELETE FROM feeds WHERE feeds.name = $1 AND feeds.destination = $2',
                                    self.name, destination.id)
            self.source.header_map.pop(destination.id)
            self.source.publish_map.pop(destination.id)

        # Return the formatted string
        return toggled

    async def emit(self) -> List[discord.Message]:

        """Handles the logic for calling dispatch method and collects any messages sent."""

        all_messages = []

        for dest in self.destinations:

            # Calls the NewsItemList dispatch method on each destination.
            sent_messages = await self.source.dispatch_items(dest, append_mode=self.append_mode)

            # Collects Message objects and updates the database with new UUIDs
            if sent_messages:
                all_messages.extend(sent_messages)
                await self.conn.execute('UPDATE feeds SET dispatched = $1 WHERE name = $2 AND destination = $3',
                                        self.source.dispatched_uuids.get(dest.id), self.name, dest.id)

        # Returns list of Messages (not currently used)
        return all_messages

    async def last(self, ctx) -> discord.Message:

        """Method exposed for sending the last item in a Feed."""

        return await self.source.send_last(ctx)


class Feeds(discord.ext.commands.Cog):

    # TODO add a cleanup hook for the emitter tasks

    def __init__(self, bot):

        self.bot = bot
        self.update_list = []
        self.delay = delay

        self.dailypuzzle_url = "https://api.chess.com/pub/puzzle"

        self.feeds: List[Feed] = [
            Feed('announcements', self.blank_fetcher, self.bot.pg_session, append_mode=True),
            Feed('chesspuzzle', self.dailypuzzle_task, self.bot.pg_session, append_mode=True)
        ]
        self.bot.tasks.append(self.emitter.start())

    @tasks.loop(seconds=delay)
    async def emitter(self):

        """Handles setup and sending loop for all Feeds."""

        await self.bot.wait_until_ready()

        for feed in self.feeds:

            if not feed.recovered:
                await feed.setup(self.bot)

            await feed.emit()

    async def test_task(self):
        return [str(datetime.datetime.now())]

    async def blank_fetcher(self) -> None:
        """Useless method used for initializing the announcements Feed."""
        return

    @commands.command(hidden=True)
    @commands.is_owner()
    async def announce(self, ctx, *, announcement):

        """Hackily injects a NewsItem into the announcements feed and then fires it immediately."""

        anc_feed = discord.utils.get(self.feeds, name='announcements')
        if anc_feed:
            item = NewsItem(announcement, max([int(u) for u in anc_feed.source.cached_uuids]) + 1)
            anc_feed.source.newsitems.append(item)
            await anc_feed.emit()
        else:
            return await ctx.send('Could not find announcements feed.')

    @commands.command()
    @checks.dmchannel_or_admin_or_permissions()
    async def subscribe(self, ctx, feed: str=None, publish: bool=False, *, header: str=''):

        """Subscribe to a specified <feed> in the channel the command is called in.
        Call with no arguments to get a list of available feeds.
        Optional arguments: <publish> and <header>.
        <publish>: false (default) will not use the Discord publish feature. True will auto-publish any feed posts.
        <header>: prepends all feed posts with the specified header string (useful for pinging roles etc.).

        Examples:
            <prefix>subscribe announcements
            <prefix>subscribe announcements true @gamers
        """

        if not feed:
            return await ctx.send(f'No feed provided. Available feeds: {", ".join([f"`{x.name}`" for x in self.feeds])}')

        feed = discord.utils.get(self.feeds, name=feed)

        if not feed:
            return await ctx.send(f'Feed not found. Available feeds: {", ".join([f"`{x.name}`" for x in self.feeds])}')

        dest = ctx.channel
        if isinstance(dest, discord.abc.PrivateChannel):
            dest = ctx.author

        toggled = await feed.toggle(dest, header, publish)
        return await ctx.send(f'Feed `{feed.name}` turned {toggled}.')

    @commands.command()
    @checks.dmchannel_or_admin_or_permissions()
    async def unsubscribe(self, ctx, feed: str=None):

        """Unsubscribe from supplied <feed>."""

        if not feed:
            return await ctx.send(f'No feed provided. Available feeds: {", ".join([f"`{x.name}`" for x in self.feeds])}')

        feed = discord.utils.get(self.feeds, name=feed)

        if not feed:
            return await ctx.send(f'Feed not found. Available feeds: {", ".join([f"`{x.name}`" for x in self.feeds])}')

        dest = ctx.channel
        if isinstance(dest, discord.abc.PrivateChannel):
            dest = ctx.author

        toggled = await feed.toggle(dest, forceoff=True)
        return await ctx.send(f'Feed `{feed.name}` turned {toggled}.')

    @commands.hybrid_command()
    async def last(self, ctx, *, feed):

        """Fetch the last thing posted in the given <feed>."""

        feed = discord.utils.get(self.feeds, name=feed)
        if not feed:
            return await ctx.send(f'Not a valid feed. Available feeds: {", ".join([f"`{x.name}`" for x in self.feeds])}')

        await feed.last(ctx)

    @commands.command(hidden=True)
    @commands.is_owner()
    async def forcefeed(self, ctx: commands.Context, destination: discord.TextChannel, feed: str, publish: bool=False, *, header: str=''):

        if not feed:
            return

        feed = discord.utils.get(self.feeds, name=feed)

        if not feed:
            return

        toggled = await feed.toggle(destination, header, publish)
        return await ctx.send(f'Feed `{feed}` turned {toggled} in `{getattr(destination.guild, "id", "None")}:{destination.id}`')

    async def dailypuzzle_task(self) -> Union[None, NewsItem]:

        """Task for chess.com daily puzzle feed. Append-mode is on."""

        await self.bot.wait_until_ready()

        # Used to check the FEN code to determine the player's color.
        fenmap = {'w': 'White', 'b': 'Black'}

        async with aiohttp.ClientSession() as sesh:
            async with sesh.get(url=self.dailypuzzle_url) as resp:

                json_resp = await resp.json()
                if not resp:
                    return
                else:
                    try:
                        # Not sure how brittle this logic is, but it seems to work.
                        turn = fenmap.get(json_resp.get('fen', '').split(' ')[1], 'White')
                    except IndexError:
                        turn = 'White'
                    if '#' in json_resp.get('pgn'):
                        win = ' and win'
                    else:
                        win = ''
                    # Formats the NewsItem and returns it.
                    return NewsItem(f'Today\'s daily chess.com puzzle is "{json_resp.get("title")}"\n`{turn} to play{win}`:', json_resp.get('publish_time'), [f'<{json_resp.get("url")}>', json_resp.get('image')])


async def setup(bot):
    await bot.add_cog(Feeds(bot))
