from html import parser as htmlparse
import discord
import datetime
import re

from ..helpers.utilities import fuzzlow

#############################
# Feeds helpers and parsers #
#############################


class Parser(htmlparse.HTMLParser):

    def handle_starttag(self, tag, attrs):
        self.data = TagWrapper(tag, attrs)

    def handle_data(self, data):
        self.data = TagWrapper('text', data)

    def handle_endtag(self, tag):
        self.data = TagWrapper(tag, None)


class TagWrapper:

    def __init__(self, tag, data):
        self.type = tag
        self.end = False

        if isinstance(data, list):
            for x in data:
                self.__dict__.update({x[0]: x[1]})

        elif data is None:
            self.end = True

        else:
            self.text_data = data

    def get(self, attr):
        if attr in self.__dict__:
            return self.__dict__[attr]
        else:
            return None


##############
# Moderation #
##############


class Citation:

    __slots__ = {'timestamp', 'reason', 'author', 'command_link'}

    def __init__(self, timestamp: datetime.datetime, reason: str, author: int, command_link: str = None):

        self.timestamp = timestamp
        self.reason = reason
        self.author = author
        self.command_link = command_link

    def __str__(self):

        return f'<@{self.author}> - {".".join(str(self.timestamp).split(".")[:-1])} - {self.reason}'


###################
# Logging helpers #
###################


class DummyMessage:

    def __init__(self, id, author):
        self.id = id
        self.author = author


class MessageWrapper:

    __slots__ = ("author", "content", "attachments", "created_at", "channel", "id", "guild", "system_content")

    def __init__(self, author, content, attachments, created_at, channel, id, guild):

        self.author = author
        self.content = content
        self.system_content = content
        self.attachments = attachments
        self.created_at = created_at
        self.channel = channel
        self.id = id
        self.guild = guild

    def censor(self, phrase: str):

        regx = re.compile(phrase, re.IGNORECASE)
        if len(phrase) > 3:
            replacement = f'{phrase[0]}{"*" * (len(phrase) - 2)}{phrase[-1]}'
        else:
            replacement = '*' * len(phrase)

        has_instances = regx.search(self.content)

        while has_instances:
            self.content = regx.sub(replacement, self.content)
            has_instances = regx.search(self.content)

        return self


class DummyAuthor:

    def __init__(self, id, mention):
        self.id = id
        self.mention = mention


class EmbedException(Exception):
    pass


class EmbedLimitExceeded(EmbedException):

    def __init__(self, message):

        super().__init__(message)


class CustomEmbed(discord.Embed):

    __slots__ = ('title', 'url', 'type', '_timestamp', '_colour', '_footer',
                 '_image', '_thumbnail', '_video', '_provider', '_author',
                 '_fields', 'description', 'char_limit', 'field_limit',
                 'title_limit', 'value_limit', 'footer_limit', 'total_chars')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.char_limit = 6000
        self.field_limit = 25
        self.title_limit = 256
        self.value_limit = 1024
        self.footer_limit = 2048
        self.total_chars = 0

        if self.title:
            if len(self.title) > self.title_limit:
                raise EmbedLimitExceeded(f'Title exceeded max length of {self.title_limit}.')
            self.total_chars += len(self.title)

        if self.description:
            if len(self.description) > self.footer_limit:
                raise EmbedLimitExceeded(f'Description exceeded max length of {self.footer_limit}.')
            self.total_chars += len(self.description)

    def __len__(self):
        total = 0
        try:
            total += len(self.title)
        except TypeError:
            pass
        try:
            total += len(self.description)
        except TypeError:
            pass

        for field in getattr(self, '_fields', []):
            total += len(field['name']) + len(field['value'])

        try:
            footer = self._footer
        except AttributeError:
            pass
        else:
            total += len(footer['text'])

        try:
            author = self._author
        except AttributeError:
            pass
        else:
            total += len(author['name'])

        return total

    def length_split(self, msg, length, wrap=None):

        out = []
        if wrap:
            padding = len(wrap(''))
            length -= padding

        while msg:

            out.append(msg[:length])
            msg = msg[length:]

        return out

    def add_field(self, *, name, value, inline=True, wrap=None):

        if isinstance(value, list):
            if wrap:
                if (len(name) + len(''.join([wrap(x) for x in value])) + len(self)) > self.char_limit:
                    raise EmbedLimitExceeded(f'Cannot add whole field without exceeding character limit of {self.char_limit}.')
            else:
                if (len(name) + len(''.join(value)) + len(self)) > self.char_limit:
                    raise EmbedLimitExceeded(f'Cannot add whole field without exceeding character limit of {self.char_limit}.')
            for f in value:
                self.add_field(name=name, value=f, inline=inline, wrap=wrap)
            return

        else:

            if wrap:
                wrapped_value = wrap(value)
            else:
                wrapped_value = value

            if len(name) > self.title_limit:

                raise EmbedLimitExceeded(f'Field name exceeded max length of {self.title_limit}.')

            if len(wrapped_value) > self.value_limit:

                self.add_field(name=name, value=self.length_split(value, self.value_limit, wrap), inline=inline, wrap=wrap)
                return

            if self.total_chars + len(name) + len(wrapped_value) > self.char_limit:

                raise EmbedLimitExceeded(f'Characters in field exceed total character limit of {self.char_limit}.')

        if len(self.fields) == 25:

            raise EmbedLimitExceeded(f'Field count exceeded max of {self.field_limit}.')


        super().add_field(name=name, value=wrapped_value, inline=inline)
        self.total_chars += len(name) + len(wrapped_value)


class EmbedPaginator:

    def __init__(self):
        self.char_limit = 6000
        self.field_limit = 25
        self.title_limit = 256
        self.value_limit = 1024
        self.footer_limit = 2048
        self.zws = u'\u200B'

    def sanitize(self, cntnt):
        return cntnt.replace("`", '`' + self.zws)

    def wrap(self, cntnt):
        return f'```{cntnt}```'

    def length_split(self, msg, length):

        out = []

        while msg:

            out.append(msg[:length])
            msg = msg[length:]

        return out

    def feed(self, msg_list, title):

        if not msg_list:
            return []

        if len(title) > self.title_limit:
            title = title[:self.title_limit]

        embed = CustomEmbed(title=title, color=discord.Color(0x000000))

        contents = []
        last_auth = msg_list[0].author
        t_content = []

        while msg_list:

            msg = msg_list.pop(0)

            if (not msg.content) & (not msg.attachments):
                continue

            if last_auth.id == msg.author.id:
                t_content += [msg]
            else:
                contents.append((last_auth, t_content))
                t_content = [msg]
                last_auth = msg.author

        contents.append((last_auth, t_content))
        had_content = False

        embeds_out = []

        for pair in contents:

            auth, block = pair

            while block:

                had_content = True

                date =          'Posted: ' + discord.utils.format_dt(block[0].created_at)
                channel =      f'Channel: {block[0].channel.mention}'
                id =           f'ID: {block[0].id}'
                auth_mention = f'Author: {block[0].author.mention}'
                if block[0].attachments:
                    atchs = 'Attachments: ' + '\n'.join([atch.proxy_url for atch in block[0].attachments])
                else:
                    atchs = ''
                val = block.pop(0).content or 'Message had no content: Attachment(s) only.'
                trailing_header = False
                trailing_body = False


                try:
                    embed.add_field(name=self.zws, value='\n'.join([auth_mention, id, channel, date, atchs]))
                    trailing_header = True
                    embed.add_field(name=self.zws, value=self.sanitize(val), inline=False, wrap=self.wrap)
                    trailing_body = True

                except EmbedLimitExceeded:

                    embeds_out.append(embed)
                    embed = CustomEmbed(title=title, color=discord.Color(0x000000))
                    if not trailing_header:
                        embed.add_field(name=self.zws, value='\n'.join([auth_mention, id, channel, date, atchs]))
                    if not trailing_body:
                        embed.add_field(name=self.zws, value=self.sanitize(val), inline=False, wrap=self.wrap)

        if not had_content:
            return []

        embeds_out.append(embed)
        return embeds_out


#############
# FuzzyDict #
#############



class FuzzProxy:
    pass


class FuzzyDict(dict):

    def get_closest_key(self, item: str, thresh: float) -> str:

        if not item:
            return FuzzProxy()

        elif item in self:
            return item

        top = []
        s = 0
        for x in self:
            if item in x:
                return x
            s_temp = fuzzlow(x, item)
            if s_temp == 100:
                return x
            elif (s_temp > s) & (s_temp >= thresh):
                try:
                    top.append((s_temp, x))
                    s = s_temp
                except KeyError:
                    pass

        if top:
            return sorted(top, reverse=True)[0][1]

        return FuzzProxy()

    def get(self, item: str, thresh: float, default: object = None) -> object:
        key = self.get_closest_key(item, thresh)
        if isinstance(key, FuzzProxy):
            return default
        return super().get(key, default)

    def substring_in(self, item: str, default: object = None):

        for key in self:

            if item.lower().strip() in key.lower().strip():
                return key

        return default
