import asyncio
from functools import partial
import re
import discord
from fuzzywuzzy import fuzz

from datetime import datetime, timedelta, timezone


def fuzzlow(s1, s2):
    return fuzz.ratio(s1.lower(), s2.lower())


def paginate(msg: str, prefix: str = '```py', suffix: str = '```'):
    pagi = discord.ext.commands.Paginator(max_size=2000, prefix=prefix, suffix=suffix)
    m_size = pagi.max_size - len(prefix) - len(suffix) - 20

    while msg:
        pagi.add_line(msg[:m_size])
        msg = msg[m_size:]

    return pagi.pages


def sanitize(msg: str):
    """
    :param msg: String to strip of any potential code injection
    :return: Clean string
    """

    return re.sub(r'[^\w\d\s\-.\[\]]', '', msg)


def escape_nick(str_in):

    if not str_in:
        return str_in

    return re.sub(r'([*~_`])', r'\\\1', str_in).replace('@', '')


def numerical_suffix(num):

    suffix_dict = {'11': 'th',
                    '12': 'th',
                    '13': 'th',
                    '1': 'st',
                    '2': 'nd',
                    '3': 'rd'}
    num = str(num)
    return suffix_dict.get(num) or suffix_dict.get(num[-1]) or 'th'


def datetimeformat(date):
    num_to_mo = {'01': 'January',
                 '02': 'February',
                 '03': 'March',
                 '04': 'April',
                 '05': 'May',
                 '06': 'June',
                 '07': 'July',
                 '08': 'August',
                 '09': 'September',
                 '10': 'October',
                 '11': 'November',
                 '12': 'December'}
    date = str(date)

    if not date:
        return 'N/A'

    try:
        d, t = date.split()
    except ValueError:
        return 'N/A'
    
    year, month, day = d.split('-')
    t = t.split('.')[0] + ' UTC'

    day += numerical_suffix(day)
    month = num_to_mo[month]

    return f'{month} {day}, {year}, {t}.'


def timestampformat(date: datetime, format: str='relative'):

    lookuptable = {
        'shorttime': '<t:{}:t>',         # 12:00 PM
        'longtime': '<t:{}:T>',          # 12:00:00 PM
        'shortdate': '<t:{}:d>',         # 1/1/01
        'longdate': '<t:{}:D>',          # January 1, 2001
        'longdateshorttime': '<t:{}:f>', # January 1, 2001 at 12:00 PM
        'dayofweekwithtime': '<t:{}:f>', # Monday, January 1, 2001 at 12:00 PM
        'relative': '<t:{}:R>',          # 23 years ago
    }

    return lookuptable.get(format, "{}").format(str(date.timestamp()).split('.')[0])


def string_to_float(inp):

    pattern = r"\d+\.?\d*"
    try:
        return re.search(pattern, inp).group()
    except IndexError:
        return ""


def durationparser(inp):

    inp = inp.lower()

    try:
        magnitude = float(string_to_float(inp))
    except ValueError:
        magnitude = 0
    except AttributeError:
        return None

    try:
        unit = re.search(r"[mhd]", inp).group()
    except AttributeError:
        return None

    if unit == "m":
        return timedelta(minutes=magnitude)
    elif unit == "h":
        return timedelta(hours=magnitude)
    elif unit == "d":
        return timedelta(days=magnitude)

    return timedelta(minutes=0)


def exec_wrapper(func):

    def wrap(*args, **kwargs):
        part = partial(func, *args, **kwargs)
        return asyncio.get_event_loop().run_in_executor(None, part)

    return wrap


def greedy_str_to_int(s: str):

    try:
        return int(s)
    except ValueError:
        return float(s)


def fuzzy_in_list(s: str, l: list):

    if s in l:
        return s

    else:
        for i in l:
            if s.lower().strip() in i.lower().strip():
                return i

    return None


def chunk_list(lst, n):

    for i in range(0, len(lst), n):
        yield lst[i:i + n]

if __name__ == "__main__":
    print(fuzzlow('a','b'))