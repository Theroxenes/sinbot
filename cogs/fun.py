import datetime
from typing import Dict, List, Union
import asyncio

from discord.ext import commands, tasks
import discord
import random
import dateparser
from .helpers.utilities import escape_nick, datetimeformat, chunk_list
from .helpers.classes import CustomEmbed
from .ext import checks
from .moderation import ButtonPaginator


class Reminder:

    __slots__ = ['text', 'when', 'ask_time', 'channel_id', 'member_id', 'jump_url', 'task']

    def __init__(self, text: str, ask_time: datetime.datetime, when: datetime.datetime, channel_id: int, member_id: int, jump_url: str):

        self.text = text
        self.ask_time = ask_time
        self.when = when
        self.channel_id = channel_id
        self.member_id = member_id
        self.jump_url = jump_url
        self.task = None

    def format_reminder_embed(self, title: str='') -> discord.Embed:
        if not title:
            title = f'Reminder set on {discord.utils.format_dt(self.ask_time)}'
        embed = discord.Embed(title=title, color=0x59d9e9)
        embed.add_field(name=f'Original Reminder: {self.jump_url}', value=self.text)
        return embed

    async def fire_reminder(self, bot: commands.Bot):

        @tasks.loop(count=1)
        async def subtask():

            await bot.wait_until_ready()
            await asyncio.sleep((self.when - datetime.datetime.now().astimezone()).total_seconds())

            chan = bot.get_channel(self.channel_id)
            user = bot.get_user(self.member_id)
            embed = self.format_reminder_embed()

            if (not chan) and user:
                try:
                    await user.send(embed=embed)
                except discord.errors.Forbidden:
                    pass

            try:
                await chan.send(f'<@{self.member_id}>', embed=embed)
            except (discord.errors.Forbidden, discord.errors.NotFound):
                try:
                    await user.send(embed=embed)
                except Exception as e:
                    print(f'Exception in reminder firing: {e}')

            await self.delete_reminder(bot.pg_session)

        self.task = subtask.start()
        bot.tasks.append(self.task)

    @staticmethod
    def parse_reminder_dur(inpstr: str) -> Union[datetime.datetime, None]:

        try:
            when = dateparser.parse(inpstr, settings={'PREFER_DATES_FROM': 'future', 'TO_TIMEZONE': 'UTC', 'RETURN_AS_TIMEZONE_AWARE': True})
        except:
            return None

        return when

    async def delete_reminder(self, pgconn):

        try:
            if self.task:
                self.task.cancel()
            await pgconn.execute('DELETE FROM reminders WHERE reminder_text = $1 AND ask_time = $2 AND remind_when = $3 AND channel_id = $4 AND user_id = $5',
                                 self.text, self.ask_time, self.when, self.channel_id, self.member_id)
        except:
            pass

    async def save_reminder(self, pgconn):

        try:
            await pgconn.execute('INSERT INTO reminders(reminder_text, ask_time, remind_when, channel_id, user_id, jump_url) '
                                 'VALUES($1, $2, $3, $4, $5, $6) ON CONFLICT DO NOTHING', self.text, self.ask_time, self.when, self.channel_id, self.member_id, self.jump_url)
        except Exception as e:
            print(e)

    def is_valid(self):
        return self.text and self.channel_id and self.member_id and self.when > datetime.datetime.now().astimezone()

    def resolve_guild(self, bot=commands.Bot) -> Union[None, discord.Guild]:

        chan = bot.get_channel(self.channel_id)
        if not chan:
            return None

        return getattr(chan, 'guild', None)

    def __str__(self):

        return f'`{self.text}` set for {self.when} on {self.ask_time} for {self.member_id} in {self.channel_id}.'

    def __repr__(self):

        return str(self)


class Fun(discord.ext.commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.beers = {}
        self.players = {}
        self.boards = []
        self.pairs = set()
        self.reminders: Dict[int, List[Reminder]] = {}
        self.zws = u'\u200B'

        self.rapesheet_urls = [
            "https://cdn.discordapp.com/attachments/650841266950635521/1120893890426306620/RTM.gif",
            "https://media.discordapp.net/attachments/867610602415194112/1092187243843686470/FrMFMoMakAICvK9.gif",
            "https://media.discordapp.net/attachments/398590278404931588/946201875370364928/5F347157-E0CF-4C4B-8B51-D317AB158FD5.gif",
            "https://cdn.discordapp.com/attachments/682401073281237017/1252272048588193953/rape.gif"
        ]
        self.minnesota_url = "https://cdn.discordapp.com/attachments/1068292484683288656/1270631190738763786/ettingermentum-1820812466793722155-01.mp4"
        self.bot.tasks.append(self.recover.start())

    @tasks.loop(count=1)
    async def recover(self):

        reminders = await self.bot.pg_session.fetch('SELECT * FROM reminders')
        resolved_reminders = [Reminder(r['reminder_text'], r['ask_time'], r['remind_when'], r['channel_id'], r['user_id'], r['jump_url']) for r in reminders]
        for rem in sorted(resolved_reminders, key=lambda r: r.when):
            if not rem.is_valid():
                await rem.delete_reminder(self.bot.pg_session)
            else:
                if rem.member_id not in self.reminders:
                    self.reminders.update({rem.member_id: []})
                self.reminders.get(rem.member_id, []).append(rem)
                await rem.fire_reminder(self.bot)

    def paginate_reminders(self, ctx: commands.Context, reminderlist: List[Reminder], *, showglobal: bool=False) -> ButtonPaginator:

        chunk_size = 5
        embeds = []

        for i, chunk in enumerate(list(chunk_list(reminderlist, chunk_size))):

            start_num = max(1, 1 + chunk_size * i)
            end_num = min(len(reminderlist), chunk_size * (i + 1))
            valid_count = 0

            embed = CustomEmbed(title=f'Reminders {start_num} to {end_num}/{len(reminderlist)} for `{ctx.author.name}`:', color=0x59d9e9)

            for j, rem in enumerate(chunk):

                if showglobal or (ctx.guild == rem.resolve_guild(ctx.bot)) or (ctx.channel.id == rem.channel_id):
                    valid_count += 1
                    embed.add_field(name=self.zws,
                                    value=f'[Reminder #{j + start_num}]({rem.jump_url}) set for {discord.utils.format_dt(rem.when)}\n'
                                          f'```\n{rem.text}```', inline=False)

            if valid_count:
                embeds.append(embed)

        if embeds:
            return ButtonPaginator(ctx.author.id, embed_pages=embeds)
        else:
            embed = CustomEmbed(title=f'No {"global" if showglobal else "server"} reminders for `{ctx.author.name}`.', color=0x59d9e9)
            return ButtonPaginator(ctx.author.id, embed_pages=[embed])

    @commands.command(hidden=True)
    async def ping(self, ctx):
        """Pong."""
        to_send = 'Greetings, heartbeat latency is {}s, send latency is {}s.'
        msg = await ctx.send(to_send.format(round(self.bot.latency, 3), 'waiting... '))
        send_latency = (msg.created_at - ctx.message.created_at).total_seconds()
        await msg.edit(content=to_send.format(round(self.bot.latency, 3), send_latency))

    @commands.command(name='8ball')
    async def eightball(self, ctx, *, question=''):

        """Ask a question, predict the future."""

        answers = [
            'It is certain',
            'It is decidedly so',
            'Without a doubt',
            'Yes – definitely',
            'You may rely on it',
            'As I see it, yes',
            'Most Likely',
            'Outlook good',
            'Yes',
            'Signs point to yes',
            'Don’t count on it',
            'My reply is no',
            'My sources say no',
            'Outlook not so good',
            'Very doubtful',
            'Reply hazy, try again',
            'Ask again later',
            'Better not tell you now',
            'Cannot predict now',
            self.minnesota_url
                ]

        if question:
            send_msg = '`{}`\n{}.'.format(question.replace('`', ''), random.choice(answers))
        else:
            send_msg = random.choice(answers)
        await ctx.send(send_msg)

    @commands.command(help='Chooses from options, split by commas.')
    async def choose(self, ctx, *, args : str):

        args = args.split(',')
        args = [x.strip().replace('`', '') for x in args]
        choice = random.choice(args)

        await ctx.send('`{}`\n`{}` was chosen.'.format(' || '.join(args), choice))

    @commands.hybrid_command(help='Get account information yourself or a specified Discord user.')
    async def userinfo(self, ctx, *, target: discord.Member = None):
        """Fetch user info and print out a pretty embed."""

        if not target:
            target = ctx.author

        try:
            ava_url = getattr(target.display_avatar, 'url', getattr(target.default_avatar, 'url', None))
        except AttributeError:
            ava_url = None

        embed = discord.Embed()
        embed.set_author(name=target.name, icon_url=target.display_avatar.url)
        embed.add_field(name='Nickname', value=escape_nick(getattr(target, 'nick', 'None')))
        embed.add_field(name='ID', value=target.id)
        embed.add_field(name='Account Creation Date', value=datetimeformat(getattr(target, 'created_at', 'N/A')), inline=False)
        embed.add_field(name='Server Join Date', value=datetimeformat(getattr(target, 'joined_at', 'N/A')), inline=False)
        embed.add_field(name='Avatar URL', value=ava_url, inline=False)

        try:
            embed.add_field(name='Roles', value=', '.join([x.name for x in target.roles if x.name != '@everyone'])[:1024]
                            or 'None', inline=False)
        except AttributeError:
            embed.add_field(name='Roles', value='N/A', inline=False)

        await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    @checks.admin_or_permissions(moderate_members=True)
    async def bean(self, ctx, member: discord.Member):

        thebean = chr(0x01fad8)
        return await ctx.send(f'{member.mention} you\'ve been beaned {thebean}.')

    @commands.command()
    @commands.guild_only()
    async def rapesheet(self, ctx):

        await ctx.send(random.choice(self.rapesheet_urls))

    @commands.command()
    async def minnesota(self, ctx):

        await ctx.send(self.minnesota_url)

    @commands.command(aliases=['reminder'])
    async def remind(self, ctx: commands.Context, *, args: str = ''):

        """Usage: <prefix>remind [time/duration], [reminder message]
        Sends the [reminder message] pinging you in the channel where it was called at [time] or after [duration].
        [time/duration] can be in almost any format. Examples: "5:00 PM EST Tomorrow", "1d", "June 10th 2030"
        Use <prefix>remind with no arguments to show existing reminders.
        Use <prefix>remind edit <x> <new text> to update an existing reminder.
        Use <prefix>remind delete <x> to remove a specific reminder."""

        # TODO clean parsing logic up, maybe use some flag hacks or subcommands?
        # TODO try greedy parsing the date/time string instead of comma separation

        args = args.replace('`', '')

        splitargs = [s.strip() for s in args.split(',')]

        user_reminders = self.reminders.get(ctx.author.id, [])

        for rem in user_reminders:
            if not rem.is_valid():
                await rem.delete_reminder(self.bot.pg_session)

        if ctx.author.id in self.reminders:
            self.reminders[ctx.author.id] = [r for r in user_reminders if r.is_valid()]

        if not any(splitargs) or args.lower() in ['show', 'global']:

            showglobal = args.lower() == 'global'
            cur_reminders = self.reminders.get(ctx.author.id)

            if not cur_reminders:
                return await ctx.send('You currently have no reminders set.')

            view = self.paginate_reminders(ctx, cur_reminders, showglobal=showglobal)
            await ctx.send(embed=view.embed_pages[0], view=view)
            return await view.wait()

        elif args.split(' ')[0].lower() in ['remove', 'delete']:

            splitargs = args.split(' ')

            if len(splitargs) != 2:
                return await ctx.send('Invalid parameters passed for `remove`, specify numerical index.')

            try:
                index_to_remove = int(splitargs[1])
                if index_to_remove < 1:
                    return await ctx.send('Reminder index must be at least 1.')
            except ValueError:
                return await ctx.send('Invalid index passed for `remove`, specify numerical index.')

            if index_to_remove > len(self.reminders.get(ctx.author.id, [])):
                return await ctx.send(f'Index out of range, you currently have {len(self.reminders.get(ctx.author.id, []))} reminders.')

            reminder_to_remove = self.reminders.get(ctx.author.id, [])[index_to_remove - 1]
            await reminder_to_remove.delete_reminder(self.bot.pg_session)
            self.reminders[ctx.author.id].pop(index_to_remove - 1)

            embed = reminder_to_remove.format_reminder_embed(title=f'Reminder #{index_to_remove} set on {discord.utils.format_dt(reminder_to_remove.ask_time)} removed')
            del reminder_to_remove

            return await ctx.send(embed=embed)

        elif args.split(' ')[0].lower() in ['edit']:

            if len(args.split(' ')) < 3:
                return await ctx.send('Invalid parameters passed for `edit`, specify numerical index and new reminder text.\n'
                                      'e.g. `remind edit 2 this is my new reminder`')

            try:
                index_to_edit = int(args.split(' ')[1])
                if index_to_edit < 1:
                    return await ctx.send('Reminder index must be at least 1.')
            except ValueError:
                return await ctx.send('Invalid index passed for `remove`, specify numerical index.')

            if index_to_edit > len(self.reminders.get(ctx.author.id, [])):
                return await ctx.send(f'Index out of range, you currently have {len(self.reminders.get(ctx.author.id, []))} reminders.')

            reminder_to_edit = self.reminders.get(ctx.author.id, [])[index_to_edit - 1]
            old_reminder_text = reminder_to_edit.text
            new_reminder_text = ' '.join(args.split(' ')[2:])

            updated_reminder = Reminder(new_reminder_text, reminder_to_edit.ask_time, reminder_to_edit.when, ctx.channel.id, ctx.author.id, ctx.message.jump_url)

            await reminder_to_edit.delete_reminder(self.bot.pg_session)
            await updated_reminder.save_reminder(self.bot.pg_session)

            self.reminders.get(ctx.author.id, []).pop(index_to_edit - 1)
            self.reminders.get(ctx.author.id, []).append(updated_reminder)
            self.reminders[ctx.author.id] = sorted(self.reminders.get(ctx.author.id, []), key=lambda r: r.when)

            await ctx.send(f'Reminder text updated from: `{old_reminder_text}`\nto: `{updated_reminder.text}`.')
            del reminder_to_edit
            return await updated_reminder.fire_reminder(self.bot)

        if len(splitargs) < 2:
            return await ctx.send('Must supply date/duration and reminder text, separated by a comma.')

        when = Reminder.parse_reminder_dur(splitargs[0])
        remindtext = ','.join(args.split(',')[1:]).strip()

        if not when:
            return await ctx.send('Invalid duration or time supplied.')
        elif not remindtext:
            return await ctx.send('No reminder text supplied.')

        reminder = Reminder(remindtext.replace('`', ''), ctx.message.created_at, when, ctx.channel.id, ctx.author.id, ctx.message.jump_url)

        if not reminder.is_valid():
            return await ctx.send('Malformed reminder, please try again.')

        if ctx.author.id not in self.reminders:
            self.reminders.update({ctx.author.id: []})

        self.reminders.get(ctx.author.id, []).append(reminder)
        self.reminders[ctx.author.id] = sorted(self.reminders.get(ctx.author.id, []), key=lambda r: r.when)
        embed = CustomEmbed(title='Reminder Set:', color=0x59d9e9)
        embed.add_field(name=self.zws, value=f'[Reminder #{self.reminders.get(ctx.author.id).index(reminder) + 1}]({reminder.jump_url}) set for {discord.utils.format_dt(reminder.when)}\n'
                                             f'```\n{reminder.text}```')

        await reminder.save_reminder(self.bot.pg_session)
        await ctx.send(embed=embed)
        return await reminder.fire_reminder(self.bot)


async def setup(bot):
    await bot.add_cog(Fun(bot))
