import datetime
import discord
from discord.ext import commands
from discord.ext.commands import clean_content as CleanText
import re
import asyncio

from .ext import checks


class Roles(discord.ext.commands.Cog):

    # TODO add sticky roles

    def __init__(self, bot):
        self.bot = bot
        self.role_dict = {}
        self.reserved_names = []
        self.bot.tasks.append(self.bot.loop.create_task(self.recover(self.bot)))
        self.role_reg = r'(`|^\+|^\-|^,|,$)'
        self.recovered = False

    @staticmethod
    def flatten_roledict(r_dict):

        out = []
        for n in r_dict:
            targ = r_dict[n]
            if isinstance(targ, dict):
                continue
            out.append(targ)

        return out

    @staticmethod
    def paginate(msg):

        pagi = discord.ext.commands.Paginator(max_size=2000, prefix='', suffix='')
        m_size = pagi.max_size - 2

        while msg:
            pagi.add_line(msg[:m_size])
            msg = msg[m_size:]

        return pagi.pages

    async def recover(self, bot):

        try:
            print('Roles cog loading.')
            t1 = datetime.datetime.now()

            await self.bot.wait_until_ready()

            conn = bot.pg_session

            #  Set up joinable roles
            rows = await conn.fetch('SELECT * FROM roles')

            if rows:

                for row in rows:
                    gid, rid, name = row
                    guild = bot.get_guild(gid)

                    if not guild:
                        continue
                    role_obj = discord.utils.get(guild.roles, id=rid)
                    if not role_obj:
                        continue

                    if gid not in self.role_dict:
                        self.role_dict.update({gid: {}})

                    g_r_dict = self.role_dict.get(gid)
                    g_r_dict.update({name: role_obj})

            self.recovered = True
            t2 = datetime.datetime.now()
            t_total = (t2 - t1).total_seconds()
            print(f'Roles cog loaded in {t_total} seconds.')
        except Exception as e:
            print(e)

    #  Cog check: ctx.guild & self.recovered
    def cog_check(self, ctx):
        return bool(ctx.guild) & self.recovered

    @commands.command()
    @checks.admin_or_permissions(manage_roles=True)
    async def role(self, ctx, name: CleanText='', *, role: discord.Role=None):

        """Sets up roles joinable by any member.
        Requires at least Manage Roles perm to call this command.
        Provide a name for the joinable and a role to assign.
        Role can be specified by ID, mention, or case-sensitive role name.
        For spaces in the [name] argument, surround with quotes.
        Calling with no args will list existing assigned joinables.
        Calling it on an existing name will edit to the provided role, or delete the joinable when no role is provided.
        Names cannot start with +, -, commas, or contain ` (grave marks)."""

        cur_roles = self.role_dict.get(ctx.guild.id)
        if not cur_roles:
            cur_roles = {}
            self.role_dict.update({ctx.guild.id: cur_roles})

        name = re.sub(self.role_reg, '', str(name))[:100]
        if not name:
            role_str = '\n'.join([f'{x}: {cur_roles[x].name.replace("`", "")}' for x in cur_roles if x not in self.reserved_names])
            for part in self.paginate(f'Current joinable roles are:```\n'
                                  f'Join Name: Role Name\n{"-" * 25}\n{role_str or "NONE"}```'):
                return await ctx.send(part)

        elif name in self.reserved_names:
            return await ctx.send(f'`{name}` is a reserved keyword.')

        if not role:
            role = cur_roles.get(name)
            if not role:
                return await ctx.send('No changes made: role not found or existing joinable not found.')

        elif role.managed:
            return await ctx.send('Integration roles cannot be assigned as joinable.')

        gid, rid = ctx.guild.id, role.id
        conn = self.bot.pg_session
        pre_exist = [r for r in self.flatten_roledict(cur_roles) if r.id == rid]

        delete = False

        if name.lower() in [r.lower() for r in cur_roles]:

            old = {k.lower(): v for k, v in cur_roles.items()}.get(name.lower())
            true_name = [x for x in cur_roles.keys() if x.lower() == name.lower()][0]

            if rid == old.id:

                delete = True
                cur_roles.pop(true_name)
                await ctx.send(f'`{true_name}: {old.name}` removed from joinable roles.')

            elif not pre_exist:
                cur_roles.update({true_name: role})
                await ctx.send(f'`{true_name}` updated to `{role.name}`.')
                await conn.execute('DELETE FROM roles WHERE guild_id = $1 AND role_id = $2', ctx.guild.id, old.id)

            else:
                return await ctx.send(f'Role `{role.name}` is already assigned to a joinable role.')


        elif pre_exist:
            return await ctx.send(f'Role `{role.name}` is already assigned to a joinable role.')

        else:

            cur_roles.update({name: role})
            await ctx.send(f'`{name}: {role.name}` added to joinable roles.')

        if not delete:

            await conn.execute('INSERT INTO roles(guild_id, role_id, name) VALUES ($1, $2, $3) '
                               'ON CONFLICT (guild_id, role_id) DO UPDATE SET role_id = $2, name = $3 '
                               'WHERE roles.guild_id = $1 AND roles.role_id = $2',
                               gid, rid, name)

        else:
            await conn.execute('DELETE FROM roles WHERE guild_id = $1 AND role_id = $2', gid, rid)

    @commands.hybrid_command()
    async def join(self, ctx, *, names: CleanText=None):

        """Add/remove one or more joinable roles to yourself.
        For multiple roles, separate their joinable names by commas.
        Call with no arguments to see available joinables.
        Roles you have will be removed from you, roles you don't have will be added."""

        joinables = {n: r for n, r in self.role_dict.get(ctx.guild.id, {}).items() if n not in self.reserved_names}
        names = [n.lower() for n in str(names).split(',')]

        if names:
            names = {re.sub(self.role_reg, '', n.strip()).strip(): re.match(self.role_reg, n.strip()) or re.match(self.role_reg, '`')
                     for n in names if re.sub(self.role_reg, '', n.strip()).strip() in [j.lower() for j in joinables.keys()]}

        if not names:
            avail = '\n'.join([f'{n}: {r.name.replace("`", "")}' for n, r in joinables.items()])
            for part in self.paginate(f'Joinable role not found. Joinable roles:```\n'
                                  f'Join Name: Role Name\n{"-" * 25}\n{avail}```'):
                return await ctx.send(part)

        final = ctx.author.roles[:]
        added = []
        removed = []

        for name in names:
            new = {k.lower(): v for k, v in joinables.items()}.get(name.lower())
            if new.id in [r.id for r in final]:
                removed.append(new)
            else:
                if new not in final:
                    added.append(new)

        final = final + added

        if not (bool(added) | bool(removed)):
            return await ctx.send('You already have all of those roles.')

        if removed:

            r = await ctx.send(f'{ctx.author.mention}, you already have the following roles: `{", ".join([r.name for r in removed])}`.\n'
                               f'React with ✅ to keep those roles or ❌ to remove them.')
            await r.add_reaction('✅')
            await r.add_reaction('❌')

            def check(reaction: discord.Reaction, react_user):

                return (react_user.id == ctx.author.id) & (reaction.emoji in ['✅', '❌'])

            try:
                reaction, user = await self.bot.wait_for('reaction_add', timeout=120, check=check)
                if reaction.emoji == '✅':
                    removed = []
            except asyncio.TimeoutError:
                await ctx.send('Reaction timed out. Keeping all roles.')
                removed = []

        final = list(set([r for r in final if r not in removed]))

        try:
            await ctx.author.edit(roles=final)
        except discord.errors.Forbidden:
            return await ctx.send('One or more roles could not be changed due to a permissions error.')

        out = 'Your roles have been updated:```diff\n'
        if added:
            out += f'+ Added:   {", ".join([r.name for r in added])}\n'
        if removed:
            out += f'- Removed: {", ".join([r.name for r in removed])}'

        out += '```'

        if bool(added) | bool(removed):
            await ctx.send(out)
        else:
            await ctx.send('No roles changed.')

    @commands.hybrid_command()
    async def leave(self, ctx, *, names: CleanText=None):

        joinables = {n: r for n, r in self.role_dict.get(ctx.guild.id, {}).items() if n not in self.reserved_names}
        names = [n.lower() for n in str(names).split(',')]

        if names:
            names = {re.sub(self.role_reg, '', n.strip()).strip(): re.match(self.role_reg, n.strip()) or re.match(
                self.role_reg, '`')
                     for n in names if
                     re.sub(self.role_reg, '', n.strip()).strip() in [j.lower() for j in joinables.keys()]}

        if not names:
            avail = '\n'.join([f'{n}: {r.name.replace("`", "")}' for n, r in joinables.items()])
            for part in self.paginate(f'Joinable role not found. Joinable roles:```\n'
                                      f'Join Name: Role Name\n{"-" * 25}\n{avail}```'):
                return await ctx.send(part)

        final = ctx.author.roles[:]
        removed = []

        for name in names:
            new = {k.lower(): v for k, v in joinables.items()}.get(name.lower())
            if new.id in [r.id for r in final]:
                removed.append(new)

        if not removed:
            return await ctx.send('You do not have any of the specified roles.')

        final = list(set([r for r in final if r not in removed]))

        try:
            await ctx.author.edit(roles=final)
        except discord.errors.Forbidden:
            return await ctx.send('One or more roles could not be changed due to a permissions error.')

        out = 'Your roles have been updated:```diff\n'
        out += f'- Removed: {", ".join([r.name for r in removed])}'

        out += '```'
        await ctx.send(out)


async def setup(bot):
    await bot.add_cog(Roles(bot))
