import asyncio
from asyncpg.exceptions import UniqueViolationError
import discord
import datetime
import re
import time as _time
from discord.ext import commands, tasks
from typing import List
import discord_markdown_ast_parser as dmap
from discord_markdown_ast_parser.parser import NodeType

from .ext import checks
from cogs.moderation import ModActionMetadata
from .helpers.classes import EmbedPaginator, CustomEmbed, MessageWrapper


class Logger(discord.ext.commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.log_modes = ['edit', 'delete', 'prune', 'update', 'leave', 'join', 'ban', 'unban', 'filter', 'timeout']
        self.log_channels = {}
        self.delete_queue = {}
        self.prune_queue = {}
        self.webhooks = {}
        self.current_prunes = []
        self.pruning = False
        self.log_timer = 0.5
        self.pagi = EmbedPaginator()
        self.zws = u'\u200B'
        self.log_lock = False
        self.nick_regex = '(.*discord\\.gg/.*)|(.*[\U000e00aa\u200b\u202b\u206b].*)'
        with open('cogs/data/ava.png', 'rb') as ava_b:
            self.ava = ava_b.read()

        self.ignore_users = [506304232937488384]

        self.bot.tasks.append(self.recover.start())
        self.try_audit_times = 5
        self.audit_delay = 0.05

        self.twitter_fixchans = {}
        self.twitter_fixbans = {}
        self.tweet_regex = re.compile(r"https?://(?:www\.)?(?:twitter\.com|x\.com|nitter\.net)/([\w_]+)/status/(\d+)(/(?:photo|video)/\d)?/?(?:\?\S+)?")
        self.fixed_url_prefix = "https://fxtwitter.com"

    def chunkems(self, l, n):
        """Yield successive n-sized chunks from l, without exceeding embed limits."""

        total = 0
        out = []
        while l:

            cur = l[0]

            if (total + len(cur) > 6000) | (len(out) == n):
                yield out
                total = len(cur)
                out = [l.pop(0)]

            else:
                total += len(cur)
                out.append(l.pop(0))

        yield out

    async def log_task(self, prune: bool, caller: discord.Member=None):

        await asyncio.sleep(1)
        self.log_lock = True

        if prune:
            queue = self.prune_queue
            log_name = 'prune'
        else:
            queue = self.delete_queue
            log_name = 'delete'

        for guild in queue:

            to_log = [m for m in queue.get(guild, []) if m.author.id not in self.ignore_users]
            if not to_log:
                continue

            elif guild in self.log_channels:

                ems = self.pagi.feed(to_log, f'Message(s) {log_name.title()}d{(" by "+caller.name) if caller else ""}:')
                chunked = list(self.chunkems(ems, 10))

                if chunked:
                    for chan in self.log_channels.get(guild, {}).get(log_name, []):

                        chan = self.bot.get_channel(chan)
                        if not chan:
                            continue

                        for chunk in chunked:
                            if not chunk:
                                continue
                            try:

                                sent = await self.webhook_send(guild, chan.id, embeds=chunk)

                                if sent is None:
                                    for em in chunk:
                                        await chan.send(embed=em)

                            except (discord.errors.Forbidden, discord.errors.HTTPException) as e:
                                print(f'Error in {log_name} logging for {guild}: {e}.\nTo Log: {to_log}\nChunked: {chunked}\nCurrent Chunk: {chunk}')

            queue.update({guild: []})

        self.log_lock = False

    async def filter_log(self, msg, phrases: list):

        chans = self.log_channels.get(msg.guild.id, {}).get('filter', [])

        if chans:

            embed = CustomEmbed(title="Filtered Message", color=0xc9b332)
            date =          'Date:\t\t   ' + msg.created_at.strftime("%Y-%m-%d %H:%M:%S")
            channel =      f'Channel:\t{msg.channel.mention}'
            id =           f'ID:\t\t\t   {msg.id}'
            auth_mention = f'Author:\t  {msg.author.mention}'
            embed.add_field(name=self.zws, value='\n'.join([auth_mention, id, channel, date]))
            embed.add_field(name="Filter Trigger(s)", value=f"`{', '.join(phrases)}`", inline=False)
            embed.add_field(name="Decensored Content", value=self.sanitize(msg.system_content), inline=False,
                            wrap=self.wrap)

        for chan in chans:

            chan = self.bot.get_channel(chan)
            if not chan:
                continue

            try:
                sent = await self.webhook_send(msg.guild.id, chan.id, embed=embed)
                if sent is None:
                    await chan.send(embed=embed)
            except (TypeError, discord.errors.HTTPException):
                return

    async def timeout_log(self, logentry: discord.AuditLogEntry = None, *, timeout: ModActionMetadata = None):

        if logentry:
            guild = logentry.guild
            reason = logentry.reason
            timeout_author = logentry.user_id
            now = logentry.created_at
            expire_time = logentry.changes.after.timed_out_until
            before_checker = logentry.changes.before.timed_out_until
            is_valid = all([expire_time is not None or before_checker is not None, expire_time or before_checker])
            if not is_valid:
                return

            if expire_time:
                duration = expire_time - now
            else:
                duration = None

            timeout = ModActionMetadata('timeout', guild.id, logentry.target.id, timeout_author, reason, now,
                                        None, duration=duration)
        elif timeout:
            guild = self.bot.get_guild(timeout.guildid)
            if timeout.duration:
                expire_time = timeout.date + timeout.duration
            else:
                expire_time = None
        else:
            return

        embed = await self.bot.modactionhandler.handle_event(timeout, self.bot, timeout_removed_manually=expire_time is None)

        await self.dispatch_log(guild.id, 'timeout', embed)

    async def ban_log(self, logentry: discord.AuditLogEntry = None, *, ban: ModActionMetadata = None):

        if logentry:
            guild = logentry.guild
            userid = getattr(logentry.target, 'id', None)
            if not userid:
                return

            ban = ModActionMetadata('ban', guild.id, userid, logentry.user_id, logentry.reason, logentry.created_at, None)
        elif ban:
            guild = self.bot.get_guild(ban.guildid)
            userid = ban.memberid
        else:
            return

        embed = await self.bot.modactionhandler.handle_event(ban, self.bot)

        await self.dispatch_log(guild.id, 'ban', embed)

        try:
            ban_metadata = await self.bot.modactionhandler.fetch_action('ban', guild.id, userid, logentry.reason)
            await self.bot.pg_session.execute(
                'INSERT INTO ban_data(user_id, guild_id, ban_reason, ban_date, ban_cmd_link, ban_author)'
                'VALUES($1, $2, $3, $4, $5, $6)'
                'ON CONFLICT (user_id, guild_id, ban_date, ban_reason) DO NOTHING',
                ban_metadata.memberid, ban_metadata.guildid, ban_metadata.reason,
                ban_metadata.date, ban_metadata.jumplink, ban_metadata.authorid)
        except UniqueViolationError:
            pass

    async def unban_log(self, logentry: discord.AuditLogEntry = None, *, unban: ModActionMetadata = None):

        if logentry:
            guild = logentry.guild
            userid = getattr(logentry.target, 'id', None)
            if not userid:
                return
        elif unban:
            guild = self.bot.get_guild(unban.guildid)
            userid = unban.memberid
        else:
            return

        unban = ModActionMetadata('unban', guild.id, userid, logentry.user_id, logentry.reason, logentry.created_at, None)
        embed = await self.bot.modactionhandler.handle_event(unban, self.bot)

        await self.dispatch_log(guild.id, 'unban', embed)

        unban_metadata = await self.bot.modactionhandler.fetch_action('unban', guild.id, userid, logentry.reason)
        await self.bot.pg_session.execute(
            'UPDATE ban_data SET tempban_resolved = $1 WHERE guild_id = $2 AND user_id = $3',
            True, unban_metadata.guildid, unban_metadata.memberid)

    async def nick_log(self, logentry: discord.AuditLogEntry = None, *, beforenick: str = False, afternick: str = False,
                       target: discord.Member = False):

        if logentry:
            beforenick = logentry.changes.before.nick or "None"
            afternick = logentry.changes.after.nick or "None"
            target = logentry.target
            author = getattr(logentry, 'user', None)
            if not author:
                author = getattr(logentry, 'target', None)
        elif beforenick is not False and afternick is not False:
            beforenick = beforenick or "None"
            afternick = afternick or "None"
            author = None
        else:
            return

        if re.match(self.nick_regex, afternick):

            try:
                await target.edit(nick=f'Bad Name #{self.bot.badboycount}')
                self.bot.badboycount += 1
            except discord.errors.Forbidden:
                return

            return

        elif re.match(self.nick_regex, beforenick):

            beforenick = 'Filtered Nickname'

        embed = discord.Embed(title=f'Member `{target.name}` Updated{f" by {author}" if author else ""}',
                              description=target.mention, color=discord.Color(0x2EFCBE))
        embed.add_field(name='Nickname Changed:', value=f'`{beforenick}` -> `{afternick}`', inline=False)

        await self.dispatch_log(target.guild.id, 'update', embed)

    async def role_log(self, logentry: discord.AuditLogEntry = None, *, b_role_ids: List[int] = False,
                       a_role_ids: List[int] = False, target: discord.Member = False):

        if logentry:
            b_role_ids = [role.id for role in logentry.changes.before.roles]
            a_role_ids = [role.id for role in logentry.changes.after.roles]
            target = logentry.target
            author = getattr(logentry, 'user', None)
            if author.id == logentry.guild.me.id or not author:
                author = target
        elif b_role_ids is not False and a_role_ids is not False and target is not False:
            author = None
        else:
            return

        embed = discord.Embed(title=f'Member `{target.name}` Updated{f" by {author}" if author else ""}',
                              description=target.mention,
                              color=discord.Color(0x2EFCBE))

        new = [f'<@&{rid}>' for rid in a_role_ids if rid not in b_role_ids]
        old = [f'<@&{rid}>' for rid in b_role_ids if rid not in a_role_ids]
        embed.add_field(name='Roles Changed', value=f'Role(s) Added: {", ".join(new) or "None"}\n'
                                                    f'Role(s) Removed: {", ".join(old) or "None"}')

        await self.dispatch_log(target.guild.id, 'update', embed)

    async def dispatch_log(self, guild_id: int, target_logtype: str, output_embed: discord.Embed):

        try:
            for chan in self.log_channels.get(guild_id, {}).get(target_logtype, []):
                chan = self.bot.get_channel(chan)
                if not chan:
                    continue

                sent = await self.webhook_send(guild_id, chan.id, embed=output_embed)
                if sent is None:
                    await chan.send(embed=output_embed)
        except (TypeError, discord.errors.HTTPException):
            return

    def walk_ast(self, nodes: List[dmap.Node]):

        links = []
        for node in nodes:

            match node.node_type:
                case NodeType.CODE_BLOCK | NodeType.SPOILER | NodeType.CODE_INLINE:
                    continue
                case NodeType.URL_WITH_PREVIEW if url := self.tweet_regex.fullmatch(node.url):
                    links.append(url)
                case _:
                    if node.children:
                        links += self.walk_ast(node.children)

        return links

    async def fix_tweet_embeds(self, message: discord.Message, url_list: list):

        fixed_urls = [f'{self.fixed_url_prefix}/{url[1]}/status/{url[2]}{url[3] or ""}' for url in url_list]

        await message.channel.send('\n'.join(list(set(fixed_urls))))

        try:
            await asyncio.sleep(1)
            await message.edit(suppress=True)
        except (discord.errors.Forbidden, discord.errors.NotFound):
            return

    @tasks.loop(count=1)
    async def recover(self):

        try:
            print('Logging (re)loading...')
            await self.bot.wait_until_ready()
            conn = self.bot.pg_session
            for mode in self.log_modes:
                await conn.execute(f'ALTER TABLE guild_prefs ADD COLUMN IF NOT EXISTS {mode}_log_channels bigint[]')
            prefs = await conn.fetch('SELECT * FROM guild_prefs')

            for pref in prefs:

                gid = pref['guild_id']
                guild = self.bot.get_guild(gid)
                cids = []

                if not guild:
                    continue

                self.log_channels.update({gid: {}})
                try:
                    self.twitter_fixchans.update({gid: pref['twitter_fixchans'] or []})
                except KeyError:
                    pass

                try:
                    self.twitter_fixbans.update({gid: pref['twitter_fixbans'] or []})
                except KeyError:
                    pass

                for mode in self.log_modes:

                    cids += pref[f'{mode}_log_channels'] or []
                    self.log_channels.get(gid, {}).update({mode: pref[f'{mode}_log_channels'] or []})

                to_remove = []
                for cid in set(cids):

                    chan = guild.get_channel(cid)
                    if not chan:
                        to_remove.append(cid)
                        continue

                    try:

                        wh_list = [w for w in (await chan.webhooks()) if w.user.id == self.bot.user.id]
                        if not wh_list:
                            wh = await chan.create_webhook(name=self.bot.user.name, avatar=self.ava)
                            print(f'Webhook(s) created for {guild.name}.')
                        else:
                            wh = wh_list[0]
                            print(f'Webhook(s) already exist for {guild.name}.')

                    except discord.errors.Forbidden:
                        print(f'No webhook(s) for {guild.name} found, cannot create new one.')
                        continue

                    except (KeyError, UnboundLocalError) as KE:
                        print(f'Bad webhook in {guild.name}:{guild.get_channel(cid).name}')
                        continue

                    if gid not in self.webhooks:
                        self.webhooks.update({gid: {}})

                    cur_whs = self.webhooks.get(gid, {})
                    if gid not in self.webhooks:
                        self.webhooks.update({gid: cur_whs})
                    cur_whs.update({cid: wh})

        except Exception as e:
            print(f'Exception in log recover: {e}')

    async def webhook_send(self, gid, cid, **kwargs):

        wh = self.webhooks.get(gid, {}).get(cid, None)
        if not wh:
            return None

        try:
            sent = await wh.send(wait=True, **kwargs)
            return sent
        except discord.errors.Forbidden:
            return None

    def wrap(self, cntnt):
        return f'```{cntnt}```'

    def sanitize(self, cntnt):
        return cntnt.replace("`", '`' + self.zws)

    async def cog_check(self, ctx):
        return bool(ctx.guild)

    @commands.command()
    @checks.admin_or_permissions()
    async def log(self, ctx, *, mode=''):

        """Toggles logging of given mode(s) in the call channel.
        Use with no arguments to see valid modes.
        To show current config do "<prefix> log show"
        To toggle multiple modes at once, either separate desired modes with commas (e.g. "<prefix> log edit, delete")
        Or use the "all" mode which will toggle all modes."""

        all_modes = self.log_modes

        if mode == 'show':
            chan = ctx.channel.id
            gid = ctx.guild.id
            out = '\n'.join([f'{mode_}: {"ON" if chan in self.log_channels.get(gid, {}).get(mode_, []) else "OFF"}' for mode_ in all_modes])
            return await ctx.send(f'```{out}```')

        if mode == 'all':
            modes = all_modes
        else:
            modes = mode.split(', ')

        if (not modes) | (len(set(modes + all_modes)) != len(all_modes)):
            return await ctx.send(f'Improper mode(s) passed. Valid modes are `{", ".join(all_modes)}, all`.')

        conn = self.bot.pg_session

        chan = ctx.channel.id

        out_str = ''
        guild_dict = self.log_channels.get(ctx.guild.id, {})
        for mode_ in modes:
            old = None
            if ctx.guild.id in self.log_channels:
                old = self.log_channels.get(ctx.guild.id, {}).get(mode_, [])

            if old:
                if chan in old:
                    old.remove(chan)
                    on = 'OFF'
                else:
                    old.append(chan)
                    on = 'ON'

            else:
                old = [chan]
                on = 'ON'

            guild_dict.update({mode_: old})

            await conn.execute(f'INSERT INTO guild_prefs(guild_id, {mode_}_log_channels) VALUES($1, $2) ON CONFLICT (guild_id) DO '
                               f'UPDATE SET {mode_}_log_channels = $2 WHERE guild_prefs.guild_id = $1', ctx.guild.id, old)

            out_str += f'{mode_.title()} logging turned {on} in this channel.\n'

        self.log_channels.update({ctx.guild.id: guild_dict})
        g_whs = self.webhooks.get(ctx.guild.id, {})
        if ctx.channel.id not in g_whs:
            try:
                wh = await ctx.channel.create_webhook(name=self.bot.user.name, avatar=self.ava)
                g_whs.update({ctx.channel.id: wh})

            except discord.errors.Forbidden:
                pass

        self.webhooks.update({ctx.guild.id: g_whs})

        await ctx.send(f'```{out_str}```')

    @commands.command(aliases=['purge'])
    @checks.admin_or_permissions(manage_messages=True)
    async def prune(self, ctx, count: int, *, args=None):

        """Prunes <count> messages from the channel it is called in.
        Can use the following flags to modify behavior:
            -before [message ID] Prunes <count> messages before the specified message.
            -after [message ID]  Prunes <count> messages after the specified message.
            -user [user ID]      Prunes <count> messages from the specified user."""

        try:
            await ctx.message.add_reaction('✅')
        except discord.errors.Forbidden:
            print(f'{ctx.author.name} started prune...')

        if count < 1:
            return await ctx.send('Cannot delete 0/negative amount of messages.')

        user, time, before, after, silent = None, None, None, None, False
        usertimeout = 10

        if args:
            for arg in args.split('-'):
                if not arg:
                    continue
                arg = arg.split()

                if arg[0] in ['before', 'after']:

                    try:
                        msg = await ctx.channel.fetch_message(arg[1])

                    except discord.errors.NotFound:
                        return await ctx.send('Before/after message not found.')

                    if arg[0] == 'before':
                        before = msg
                    else:
                        after = msg

                elif arg[0] == 'user':
                    try:
                        user = discord.utils.get(ctx.guild.members, id=int(arg[1]))
                    except ValueError:
                        user = discord.utils.get(ctx.guild.members, mention=arg[1])
                    if not user:
                        return await ctx.send('User not found.')

                elif arg[0] == 'silent':

                    silent = True

        else:
            count += 1

        to_delete = []
        deleted = []

        def sanity_check_check(message):

            return (message.author.id == ctx.author.id) & (message.channel.id == ctx.channel.id)

        async def sanity_check(s_count: int):

            confirmed = False
            await ctx.send(f'{ctx.author.mention}, are you sure you want to delete {s_count} messages?')
            try:
                msg = await ctx.bot.wait_for('message', check=sanity_check_check, timeout=10)
                if msg.content.lower() in ['yes', 'y']:
                    confirmed = True
                else:
                    await ctx.send('Aborting prune.')
            except asyncio.TimeoutError:
                await ctx.send(f'{ctx.author.mention}, confirmation timed out.')

            return confirmed

        if user:

            start = datetime.datetime.now(tz=datetime.timezone.utc)
            i = 0

            async for message in ctx.channel.history(limit=None, before=before, after=after):

                i += 1

                if message.author.id == user.id:
                    to_delete.append(message)
                    self.current_prunes.append(message.id)

                if len(to_delete) >= count or (
                        datetime.datetime.now(tz=datetime.timezone.utc) - start).total_seconds() > usertimeout:
                    await ctx.send(
                        f'Iterated through {i} messages in {round((datetime.datetime.now(tz=datetime.timezone.utc) - start).total_seconds(), 2)} seconds, found {len(to_delete)} valid target(s). Pruning.')
                    break

            if len(to_delete) > 100:
                conf = await sanity_check(len(to_delete))
                if not conf:
                    return

            while count:
                r = min(100, count)
                try:
                    await ctx.channel.delete_messages(to_delete[:r])
                except discord.errors.Forbidden:
                    return await ctx.send('Insufficient bot permissions to perform prune.')
                deleted += to_delete[:r]
                to_delete = to_delete[r:]
                count -= min(100, count)

        else:
            if count > 100:
                conf = await sanity_check(count)
                if not conf:
                    return
            try:
                deleted = await ctx.channel.purge(limit=count, before=before, after=after, oldest_first=bool(after))
            except discord.errors.Forbidden:
                return await ctx.send('Insufficient bot permissions to perform prune.')
            self.current_prunes += [m.id for m in deleted]

        while self.log_lock:
            await asyncio.sleep(0.1)

        prev = self.prune_queue.get(ctx.guild.id, [])
        prev += sorted(deleted, key=lambda m: m.created_at)
        self.prune_queue.update({ctx.guild.id: prev})
        await self.log_task(True, caller=ctx.author)

        if not silent:
            try:
                return await ctx.send(f'{len(deleted)} message(s) pruned.')
            except (discord.errors.Forbidden, discord.errors.HTTPException):
                return

    @commands.command(aliases=['clean'])
    @checks.admin_or_permissions(manage_messages=True)
    async def clear(self, ctx, user=discord.ext.commands.parameter(description="Username or user ID to clear"),
                    time: int = discord.ext.commands.parameter(default=0,
                                                               description="Time in minutes for how far back to clear"),
                    dryrun: bool = discord.ext.commands.parameter(default=False,
                                                                  description="Do dry run if True, not actually deleting messages.")):

        """Clears all messages in all channels from <user> posted in the past <time> minutes.
        Note: long clear times may take a while to complete."""

        def sanity_check_check(message):

            return (message.author.id == ctx.author.id) & (message.channel.id == ctx.channel.id)

        async def sanity_check(confirmation_message: str):

            confirmed = False
            await ctx.send(confirmation_message)
            try:
                msg = await ctx.bot.wait_for('message', check=sanity_check_check, timeout=10)
                if msg.content.lower() in ['yes', 'y']:
                    confirmed = True
                else:
                    await ctx.send('Aborting clear.')
            except asyncio.TimeoutError:
                await ctx.send(f'{ctx.author.mention}, confirmation timed out.')

            return confirmed

        try:
            user_resolved = await commands.UserConverter().convert(ctx, user)
            userid = user_resolved.id
        except commands.UserNotFound:
            try:
                userid = int(user)
                confirmation = await sanity_check(f'User not resolved, proceed with bare ID `{userid}`? (y/n)')
                if not confirmation:
                    return
            except ValueError:
                return await ctx.send('User not found, and valid ID not provided. Aborting.')

        try:
            await ctx.message.add_reaction('✅')
        except discord.errors.Forbidden:
            print(f'{ctx.author.name} started clear...')

        if time < 1:
            return await ctx.send('Time must be greater than 0.')

        cur_time = datetime.datetime.now()
        earliest_time = cur_time - datetime.timedelta(minutes=time)
        to_delete = []
        deleted = []
        chan_fails = []
        usertimeout = datetime.timedelta(minutes=10)

        text_chans = [chan for chan in ctx.guild.channels if chan.type in
                      [discord.ChannelType.text, discord.ChannelType.public_thread,
                       discord.ChannelType.private_thread]]
        orig_len = len(text_chans)
        text_chans = [chan for chan in text_chans if chan.permissions_for(ctx.me).manage_messages and
                      chan.permissions_for(ctx.me).read_message_history and
                      chan.permissions_for(ctx.me).read_messages]
        status_embed = discord.Embed(title="Clear Status:",
                                     description=f"Clearing messages from `{getattr(user_resolved, 'name', userid)}` after {discord.utils.format_dt(earliest_time)}.")
        status_embed.add_field(name="Current Channel",
                               value="None (0/{len(text_chans)})", inline=False)
        status_embed.add_field(name="Cleared Messages", value="None.",
                               inline=False)
        status_embed.set_footer(text=f'Missing permissions in {orig_len - len(text_chans)} channel(s).')
        status_msg = await ctx.send(embed=status_embed)
        chan_count = 0
        msg_count = 0
        purged = 0

        def purge_check(message):
            return (message.author.id == userid) and (message.id != status_msg.id)

        for channel in text_chans:

            if channel.type not in [discord.ChannelType.text, discord.ChannelType.public_thread,
                                    discord.ChannelType.private_thread]:
                continue

            chan_count += 1
            status_embed.set_field_at(0, name="Current Channel",
                                      value=f"{channel.name} ({chan_count}/{len(text_chans)})", inline=False)
            await status_msg.edit(embed=status_embed)

            try:

                if not dryrun:
                    # self.pruning = True

                    to_log = (await channel.purge(limit=None, check=purge_check, before=cur_time, after=earliest_time,
                                                  reason=f"Clear command invoked by {ctx.author.name}."))

                    prev = self.prune_queue.get(ctx.guild.id, [])
                    prev += sorted(to_log, key=lambda m: m.created_at)
                    self.prune_queue.update({ctx.guild.id: prev})
                    self.current_prunes += [m.id for m in to_log]
                    await self.log_task(True, caller=ctx.author)
                    purged += len(to_log)

                status_embed.set_field_at(1, name="Cleared Messages", value=f"{len(deleted) + purged} cleared.",
                                          inline=False)
                try:
                    await status_msg.edit(embed=status_embed)
                except discord.errors.NotFound:
                    status_msg = await ctx.send(embed=status_embed)

            except (discord.errors.Forbidden, discord.errors.NotFound):

                try:
                    async for message in channel.history(limit=None, after=earliest_time):

                        # self.pruning = True
                        msg_count += 1

                        if purge_check(message):
                            to_delete.append(message)
                            self.current_prunes.append(message.id)
                            if len(status_embed.fields) < 4:
                                status_embed.add_field(name="Manual Deletes", value="0 marked for clear, 0 searched",
                                                       inline=False)
                            status_embed.set_field_at(3, name="Manual Deletes",
                                                      value=f"{len(to_delete) + purged} marked for clear, {msg_count} searched.",
                                                      inline=False)
                            try:
                                await status_msg.edit(embed=status_embed)
                            except discord.NotFound:
                                status_msg = await ctx.send(embed=status_embed)

                        if (datetime.datetime.now() - cur_time).total_seconds() > usertimeout.total_seconds():
                            await ctx.send(
                                f'Iterated through {msg_count} messages in {(datetime.datetime.now() - cur_time).total_seconds()} seconds, found {len(to_delete)} valid target(s). Clearing.')
                            # self.pruning = False
                            break

                    # self.pruning = False

                except discord.errors.Forbidden:

                    chan_fails.append(channel.name)
                    status_embed.set_footer(
                        text=f'Missing permissions in {len(chan_fails) + orig_len - len(text_chans)} channel(s).')
                    try:
                        await status_msg.edit(embed=status_embed)
                    except discord.NotFound:
                        status_msg = await ctx.send(embed=status_embed)

                except Exception as exc:
                    chan_fails.append(channel.name)
                    await ctx.send(f'Unhandled error while clearing messages from {channel.jump_url}: {exc}')

            except Exception as exc:
                chan_fails.append(channel.name)
                await ctx.send(f'Unhandled error while clearing messages from {channel.jump_url}: {exc}')

        if len(to_delete) > 100:
            conf = await sanity_check(
                f'{ctx.author.mention}, are you sure you want to delete {len(to_delete)} messages?')
            if not conf:
                return

        if not dryrun:
            for message in to_delete:
                try:
                    await message.delete(reason=f"Clear command invoked by {ctx.author.name}.")
                    deleted.append(message)
                except (discord.errors.NotFound, discord.errors.Forbidden):
                    pass

        while self.log_lock:
            await asyncio.sleep(0.1)

        if not dryrun:
            self.current_prunes += [m.id for m in deleted]
            prev = self.prune_queue.get(ctx.guild.id, [])
            prev += sorted(deleted, key=lambda m: m.created_at)
            self.prune_queue.update({ctx.guild.id: prev})
            await self.log_task(True, caller=ctx.author)
        else:
            await ctx.send("Dry run, no messages deleted.")

        try:
            return await ctx.send(
                f'Iterated through{f" {msg_count} message(s) in" if msg_count else ""} {len(text_chans)} channel(s).\n{len(deleted) + purged} message(s) cleared.')
        except (discord.errors.Forbidden, discord.errors.HTTPException):
            return

    @commands.command()
    @checks.admin_or_permissions(manage_messages=True)
    async def twitterfix(self, ctx: commands.Context, *, args: str):

        args = args.split(' ')

        if args[0].lower() == 'ban':
            try:
                target = (await commands.MemberConverter().convert(ctx, argument=args[1])).id
            except Exception as e:
                await ctx.send(f'Error converting member {args[1]} to a guild member: {e}.\nBanning raw ID.')
                try:
                    target = int(args[1])
                except ValueError:
                    return await ctx.send(f'Unable to convert ban target to a valid ID.')

            if not self.twitter_fixbans.get(ctx.guild.id):
                self.twitter_fixbans.update({ctx.guild.id: []})

            if target not in self.twitter_fixbans.get(ctx.guild.id, []):
                self.twitter_fixbans[ctx.guild.id].append(target)
                action = 'banned'
            else:
                self.twitter_fixbans[ctx.guild.id].remove(target)
                action = 'unbanned'

            await self.bot.pg_session.execute('UPDATE guild_prefs SET twitter_fixbans = $1 WHERE guild_id = $2',
                                              self.twitter_fixbans.get(ctx.guild.id, []), ctx.guild.id)
            return await ctx.send(f'Member {target} {action} from twitter link fixing.')

        if ctx.guild.id not in self.twitter_fixchans:
            self.twitter_fixchans.update({ctx.guild.id: []})
        cur_chans = self.twitter_fixchans.get(ctx.guild.id, [])

        if ctx.channel.id in cur_chans:
            new_state = 'off'
            self.twitter_fixchans[ctx.guild.id].remove(ctx.channel.id)
        else:
            new_state = 'on'
            self.twitter_fixchans[ctx.guild.id].append(ctx.channel.id)

        await self.bot.pg_session.execute('UPDATE guild_prefs SET twitter_fixchans = $1 WHERE guild_id = $2',
                                          self.twitter_fixchans[ctx.guild.id], ctx.guild.id)
        await ctx.send(f'Twitter fixing turned {new_state} in this channel.')

    @commands.Cog.listener()
    async def on_message_delete(self, message):

        if not message.guild:
            return
        else:
            guild = message.guild

        while self.log_lock | self.pruning:
            await asyncio.sleep(0.1)

        prev = self.delete_queue.get(guild.id, [])
        filtered_phrases = []

        if message.id not in self.current_prunes:
            if message.id in self.bot.filtered_messages:
                message = MessageWrapper(message.author, message.system_content, message.attachments,
                                         message.created_at, message.channel, message.id, message.guild)
                for phrase in self.bot.filter.get(guild.id):
                    if phrase.lower() in message.content.lower():
                        filtered_phrases.append(phrase)
                        message.censor(phrase)
                await self.filter_log(message, filtered_phrases)

            prev.append(message)
            self.delete_queue.update({guild.id: prev})
            await self.log_task(False)

        else:
            self.current_prunes.remove(message.id)

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload):
        pass

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):

        if (not before.guild) | (before.system_content == after.system_content) | (after.author.id in self.ignore_users):
            return

        if before.guild.id in self.log_channels:
            embed = CustomEmbed(title='Message Edited', color=discord.Color(0xFCCC2F))
            date =          'Edited At:\t\t   ' + discord.utils.format_dt(datetime.datetime.utcnow())
            channel =      f'Channel:\t{before.channel.mention}'
            id =           f'ID:\t\t\t   {before.id}'
            auth_mention = f'Author:\t  {before.author.mention}'
            embed.add_field(name=self.zws, value='\n'.join([auth_mention, id, channel, date]))
            embed.add_field(name="Direct Message Link", value=before.jump_url, inline=False)
            embed.add_field(name='Original Content', value=self.sanitize(before.system_content), inline=False, wrap=self.wrap)
            embed.add_field(name='New Content', value=self.sanitize(after.system_content), inline=False, wrap=self.wrap)
            try:
                for chan in self.log_channels.get(before.guild.id, {}).get('edit', []):
                    chan = self.bot.get_channel(chan)
                    if not chan:
                        continue

                    sent = await self.webhook_send(before.guild.id, chan.id, embed=embed)
                    if sent is None:
                        await chan.send(embed=embed)
            except (TypeError, discord.errors.HTTPException):
                return

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):

        if message.author.bot or not message.content or not message.channel or not message.guild \
         or message.channel.id not in self.twitter_fixchans.get(message.guild.id, [])\
                or message.author.id in self.twitter_fixbans.get(message.guild.id, []):
            return

        try:
            twits = self.walk_ast(dmap.parse(message.content))
        except TypeError:
            return
        if not twits:
            return

        await self.fix_tweet_embeds(message, twits)

    @commands.Cog.listener()
    async def on_member_join(self, member):

        embed = discord.Embed(title=f'Member `{member.name}` joined guild:', description=member.mention,
                              color=discord.Color(0x049300))

        if re.match(self.nick_regex, member.display_name) or re.match(self.nick_regex, member.name):
            embed.description = f'Bad Username: ID {member.id}'

        try:
            for chan in self.log_channels.get(member.guild.id, {}).get('join', []):
                chan = self.bot.get_channel(chan)
                if not chan:
                    continue

                sent = await self.webhook_send(member.guild.id, chan.id, embed=embed)
                if sent is None:
                    await chan.send(embed=embed)
        except (TypeError, discord.errors.HTTPException):
            return

    @commands.Cog.listener()
    async def on_member_remove(self, member):

        embed = discord.Embed(title=f'Member `{member.name}` left guild:', description=member.mention,
                              color=discord.Color(0xB777A8))

        if re.match(self.nick_regex, member.display_name) or re.match(self.nick_regex, member.name):
            embed.description = f'Bad Username: ID {member.id}'

        try:
            for chan in self.log_channels.get(member.guild.id, {}).get('leave', []):
                chan = self.bot.get_channel(chan)
                if not chan:
                    continue

                sent = await self.webhook_send(member.guild.id, chan.id, embed=embed)
                if sent is None:
                    await chan.send(embed=embed)
        except (TypeError, discord.errors.HTTPException):
            return

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):

        now = datetime.datetime.now(tz=datetime.timezone.utc)

        if after.guild.me.guild_permissions.view_audit_log:
            return

        beforenick = before.nick
        afternick = after.nick
        b_role_ids = [role.id for role in before.roles]
        a_role_ids = [role.id for role in after.roles]

        if beforenick != afternick:
            await self.nick_log(beforenick=beforenick, afternick=afternick, target=after)
        elif b_role_ids != a_role_ids or before.nick != after.nick:
            await self.role_log(b_role_ids=b_role_ids, a_role_ids=a_role_ids, target=after)
        elif getattr(before, 'timed_out_until', None) != getattr(after, 'timed_out_until', None):
            if getattr(after, 'timed_out_until', None):
                duration = after.timed_out_until - now
            else:
                duration = None
            timeout = ModActionMetadata('timeout', after.guild.id, after.id, "Unknown",
                                        "Could not retrieve timeout reason.", now, duration=duration)
            await self.timeout_log(timeout=timeout)

    @commands.Cog.listener()
    async def on_member_ban(self, guild: discord.Guild, user: discord.User):

        if guild.me.guild_permissions.view_audit_log:
            return

        ban_author = None
        now = datetime.datetime.now()
        try:
            ban_event = [b async for b in guild.bans(limit=10) if b.user.id == user.id][0]
            reason = ban_event.reason
        except (IndexError, discord.errors.Forbidden):
            reason = "Could not retrieve ban reason."

        ban = ModActionMetadata('ban', guild.id, user.id, ban_author, reason, now, None)
        embed = await self.bot.modactionhandler.handle_event(ban, self.bot)

        await self.dispatch_log(guild.id, 'ban', embed)

        try:
            ban_metadata = await self.bot.modactionhandler.fetch_action('ban', guild.id, user.id, reason)
            await self.bot.pg_session.execute('INSERT INTO ban_data(user_id, guild_id, ban_reason, ban_date, ban_cmd_link, ban_author)'
                                              'VALUES($1, $2, $3, $4, $5, $6)'
                                              'ON CONFLICT (user_id, guild_id, ban_date, ban_reason) DO NOTHING',
                                              ban_metadata.memberid, ban_metadata.guildid, ban_metadata.reason,
                                              ban_metadata.date, ban_metadata.jumplink, ban_metadata.authorid)
        except UniqueViolationError:
            pass

    @commands.Cog.listener()
    async def on_member_unban(self, guild, user):

        now = datetime.datetime.now()
        if guild.me.guild_permissions.view_audit_log:
            return

        reason = 'Could not find unban audit log entry.'
        unban_author = None

        unban = ModActionMetadata('unban', guild.id, user.id, unban_author, reason, now, None)
        embed = await self.bot.modactionhandler.handle_event(unban, self.bot)

        try:
            for chan in self.log_channels.get(guild.id).get('unban', []):
                chan = self.bot.get_channel(chan)
                if not chan:
                    continue

                sent = await self.webhook_send(guild.id, chan.id, embed=embed)
                if sent is None:
                    await chan.send(embed=embed)
        except (TypeError, discord.errors.HTTPException):
            return

        unban_metadata = await self.bot.modactionhandler.fetch_action('unban', guild.id, user.id, reason)
        await self.bot.pg_session.execute('UPDATE ban_data SET tempban_resolved = $1 WHERE guild_id = $2 AND user_id = $3',
                                          True, unban_metadata.guildid, unban_metadata.memberid)

    @commands.Cog.listener()
    async def on_audit_log_entry_create(self, logentry: discord.AuditLogEntry):

        entrytype = logentry.action
        if entrytype is discord.AuditLogAction.member_update:
            if getattr(logentry.before, 'nick', False) != getattr(logentry.after, 'nick', False):
                await self.nick_log(logentry)
            elif getattr(logentry.before, 'timed_out_until', False) != getattr(logentry.after, 'timed_out_until', False):
                await self.timeout_log(logentry)
        elif entrytype is discord.AuditLogAction.member_role_update:
            await self.role_log(logentry)
        elif entrytype is discord.AuditLogAction.ban:
            await self.ban_log(logentry)
        elif entrytype is discord.AuditLogAction.unban:
            await self.unban_log(logentry)


async def setup(bot):
    await bot.add_cog(Logger(bot))
