## COMMANDS

Get the help text of any command with `help <commandname>`  
`<>` arguments are required, `[]` arguments are optional. The brackets are not part of the argument.  
Each Moderation, Logger, and Roles command has an associated permission which is also documented below.



## Bot Permissions
The bot requires `View Channels`, `Send Messages`, `Attach Files`, `Embed Files` to actually interact with users.  
`ban/forceban/tempban/unban` and `timeout` require `Ban Members` and `Timeout Members` respectively.  
`silence` and `join/leave` require `Manage Roles`.  
`clear`, `prune`, and `filter` require `Manage Messages`.  
`yoink` requires `Create Expressions`.  
While not required, for full logging capabilities the bot requires `View Audit Log` permissions.  

The minimum permission set for full functionality is:  
> `View Channels`  
> `Send Messages`  
> `Attach Files`  
> `Embed Files`  
> `Ban Members`  
> `Timeout Members`  
> `Manage Roles`  
> `Manage Messages`  

Optional Permissions:  
> `View Audit Log` for more verbose logging of moderation commands and events.  
> `Create Expressions` for the yoink command to function.



### Moderation

> `ban <user> [-reason <reason>] [-delete <deletetime>]`  
> Required permission: Ban Members  
> Bans the specified member (user mention, full username, or ID) permanently with the supplied reason and appends to citations.  
> `-reason` flag can be omitted as long as it is the first argument after `<user>`.  
> `<deletetime>` deletes messages in the specified duration as `Xd`, `Xh`, or `Xm` for days, hours, minutes respectively.  

> `forceban <user> [-reason <reason>] [-delete <deletetime>]`    
> Required permission: Ban Members  
> Same as the `ban` command but accepts user ID only and can ban non-members.  

> `tempban <member> <time (Xd/Xh/Xm)> [-reason <reason>]`    
> Required permission: Ban Members  
> Bans the specified member for the specified `<time>` as `Xd`, `Xh`, or `Xm` for days, hours, minutes respectively and appends to citations.  
> `-reason` flag can be omitted as long as it is the last positional argument.  

> `unban <user> [reason]`    
> Required permission: Ban Members  
> Unbans the user specified by ID, username, or user mention.  
> Reason is optional.

> `timeout <member> <time (minutes)> [reason]`
> Required permission: Timeout Members  
> Aliases: `mute`  
> Times the specified member out for `<time>` minutes and appends to citations.  
> Optionally, provide the time in `Xd/Xh/Xm` format as for other commands.  
> Use `<time>` of 0 to remove a timeout.

> `citation [user] [message | clear | remove <id> | edit <id> <new>]`      
> Required permission: Timeout Members (or None to view your own citations)  
> Aliases: `rapsheet`  
> Manually add a timestamped citation to the user's citations.  
> Call with no `[message]` to view current citations and no `[user]` to view your own.  
> To remove a citation, use: `citation <user> remove <id>` where `<id>` is the # number shown on the citation entry.  
> To edit a citation, use: `citation <user> edit <id> <new>` where <id> is the # number shown on the citation entry.  
> To clear all citations you have permission to remove, do: `citation <user> clear.`  
> Server owner can remove/edit any citation. Server administrators can remove/edit their own citations and non-owner/non-admin citations.  
> Everyone else can only remove/edit citations they created.

> `silence <time (minutes)>`      
> Required permission: Manage Channels  
> Prevents anyone from talking in the called channel for `<time>` minutes.  
> Cancel early by posting "resume" in the called channel.

> `togglecommand <commandname>`        
> Required permission: Manage Channels  
> Enables/disables the specified command in the called channel.  

> `filter [word]`      
> Required permission: Administrator  
> Adds a word to the server filter which will be auto-deleted and logged with filtered content censored.  
> Call with no arguments to view current filter.  

> `set_prefix <prefix>`        
> Required permission: Administrator  
> Sets a custom bot prefix for this server.  

> `set_welcome <welcomemessage>`        
> Required permission: Administrator  
> Sets a welcome message to be posted to the channel this command is called in when a new member joins.  
> `$usermention$` and `$username$` in `<welcomemessage>` can be used to insert a mention or the user's name respectively.  

> `yoink <message ID/link> <name>`  
> Required permission: Create Expressions  
> Adds the first sticker or emoji in the target `<message ID/link>` to the server with a name of `<name>`.




### Logger

> `clear <user> <time (minutes)> [dryrun (true or false)]`        
> Required permission: Manage Messages  
> Aliases: `clean`  
> Deletes all messages from the specified user in all channels posted in the last `<time>` minutes.  
> Specify `[dryrun]` to run the command without actually deleting anything, defaults to `false`.  

> `prune <count> [-before message ID] [-after message ID] [-user user]`        
> Required permission: Manage Messages  
> Aliases: `purge`  
> Removes `<count>` messages from `<user>` in called channel. Uses prune logging mode rather than delete.  
> Can specify `-before message ID`, `-after message ID`, and/or `-user user` to fine-tune what is pruned.  
> All flags can be used simultaneously.  

> `log [logmodes]`  
> Required permission: Administrator  
> Enables/disables logging of the specified mode(s) in the called channel. To specify multiple modes separate them with a comma.  
> Modes are: `edit`, `delete`, `prune`, `update`, `leave`, `join`, `ban`, `unban`, `filter`.  
> Can also specify `all` as the mode to enable all logging.  
> Call with `show` as the mode to show currently enabled logging modes, or with no mode to show all logging modes.  
> NOTE: "View Audit Log" permissions are needed for the bot to have full logging capability.  

> `twitterfix [ban] [member]`  
> Required permission: Manage Messages  
> Enables or disables fixing twitter embeds by sending an fxtwitter.com link and stripping the embed from the original message.  
> Use with `[ban] [member]` to block a specified member's links from being fixed (or unban someone who is already banned).



### Roles

> `role <shortname> <rolename>`  
> Required permission: Manage Roles  
> Adds `<rolename>` to the list of joinable roles, users use `<shortname>` to join or leave it.  
> `<rolename>` is case-sensitive while `<shortname>` is not.  

> `join [shortname]`  
> Required ermission: None  
> Joins you to the specified joinable role.  
> Call with no shortname to view joinable roles.  
> If used on a role you already have, will prompt to remove it.

> `leave [shortname]`  
> Required permission: None  
> Removes you from the specified joinable role.  
> Call with no shortname to view joinable roles.  



### Fun

> `userinfo [user]`  
> Required permission: None  
> Provides info on the specified user, or yourself if called with no argument.  

> `remind <duration/time>, <message>`  
> OR  
> `remind delete <index>`  
> OR  
> `remind edit <index> <new message>`  
> Required permission: None  
> Sets a reminder. Specify `<duration/time>` in just about any format.  
> Use `remind` with no argument to post current reminders for the current server.  
> Use `remind delete <index>` to remove an existing reminder.  
> Use `remind edit <index> <new message>` to update an existing reminder.  
> The jump-link, text, and channel it is sent to will be updated.  
> Use `remind global` to view all reminders across all servers and reminders set in DMs.

> `8ball [message]`  
> Required Permissions: None  
> Predicts the future.  

> `choose <option1>, [option2]`  
> Required permission: None  
> Helps you make a decision, entirely randomly.  

> `bean <user>`  
> Required permission: Timeout Members  
> Beans the `<user>`.  

> `rapesheet`  
> Required permission: None  
> rapethisman.gif



### Feeds  

> `subscribe [feed] [publish] [header]`  
> Required permission: Admin (or None in a DM).  
> Sets the channel the command is called in as a destination for `[feed]` posts.  
> `[publish]` option will automatically use Discord's publish feature on each feed post.  
> `[header]` option will prepend each feed post with the specific text header (i.e. a role mention).  
> Call with no arguments to get a list of available feeds.  

> `unsubscribe [feed]`  
> Unsubscribes the channel the command is called in from the specified `<feed>`. 

> `last <feed>`  
> Displays the most recent post from `<feed>` (doesn't need to be subscribed in the call channel).